enum am {
	/* Part of Base ISA. */
	IMM,	/* Immediate Data. */
	ZM,	/* Zero Matrix. */
	ZMX,	/* Zero Matrix, indexed with X. */
	ZMY,	/* Zero Matrix, indexed with Y. */
	IND,	/* Indirect. */
	INDX,	/* Indexed Indirect. */
	INDY,	/* Indirect Indexed. */
	ABS,	/* Absolute. */
	REL,	/* Relative to Program Counter. */
	BREG,	/* B Register. */
	IMPL,	/* Implied. */
	/* Part of Base Extension. */
	ABSX,	/* Absolute, Indexed with X. */
	ABSY,	/* Absolute, Indexed with Y. */
	AIND,	/* Absolute Indirect. */
	AINDX,	/* Absolute Indexed Indirect. */
	AINDY,	/* Absolute Indirect Indexed. */
	EIND,	/* Effective Address Register, Indirect. */
};

/* Part of the Orthogonal Extension. */
enum ortho_reg {
	REG_A,
	REG_B,
	REG_X,
	REG_Y,
	REG_E,
	REG_C,
	REG_D,
	REG_S,
	REG_F,
	REG_SP,
	REG_BP,
	REG_R11,
	REG_R12,
	REG_R13,
	REG_R14,
	REG_R15,
	REG_PC,
};

enum ortho_mem {
	MEM_ABS,	/* Absolute. */
	MEM_ZM,		/* Zero Matrix. */
	MEM_ABSR,	/* Absolute, Indexed with register. */
	MEM_ZMR,	/* Zero Matrix, Indexed with register. */
	MEM_AIND,	/* Absolute Indirect. */
	MEM_IND,	/* Zero Matrix Indirect. */
	MEM_AINDR,	/* Absolute, Indirect Indexed Register. */
	MEM_ZINDR,	/* Zero Matrix, Indirect Indexed Register. */
	MEM_ARIND,	/* Absolute, Indexed Indirect Register. */
	MEM_ZRIND,	/* Zero Matrix, Indexed Indirect Register. */
	MEM_RIND,	/* Register Indirect. */
	MEM_IMM,	/* Immediate Data. */
	MEM_SIB = 0xE,	/* Scale Index Base. */
};

enum mne {
	ADC,
	AND,
	ASR,
	BCC,
	BCS,
	BEQ,
	BNE,
	BNG,
	BPO,
	BRA,
	BRK,
	BVC,
	BVS,
	CLC,
	CLI,
	CLV,
	CMP,
	CPB,
	CPS,
	CPX,
	CPY,
	DEB,
	DEC,
	DEX,
	DEY,
	DIV,
	INB,
	INC,
	INX,
	INY,
	JMP,
	JSR,
	LDA,
	LDB,
	LDX,
	LDY,
	LSL,
	LSR,
	MUL,
	NOP,
	ORA,
	PHA,
	PHB,
	PHP,
	PHX,
	PHY,
	PLA,
	PLB,
	PLP,
	PLX,
	PLY,
	ROL,
	ROR,
	RTI,
	RTS,
	SBC,
	SEC,
	SEI,
	STA,
	STB,
	STX,
	STY,
	TAB,
	TAX,
	TAY,
	TBA,
	TSX,
	TXA,
	TXS,
	TXY,
	TYA,
	TYX,
	WAI,
	XOR
};

enum ext_mne {
	LEA,
	PEA,
	ADD,
	SUB,
	ADE,
	SBE,
	ADS,
	SBS,
	NOT,
	LLM,
	LRM,
	RLM,
	RRM,
	ARM,
	PHE,
	PLE,
	CPE,
	ICE,
	LDS,
	DEE,
	INE,
	DES,
	INS,
	STS,
	STE,
	STZ,
	SCO,
	ECO,
	CLZ,
	CLO,
	BIT,
	MMV,
	SWP,
	PCN,
	REP,
	REQ,
	RNE,
	LNG,
	LPO,
	LCS,
	LCC,
	LEQ,
	LNE,
	SNG,
	SPO,
	SCS,
	SCC,
	SEQ,
	SNE
};

enum ortho_mne {
	MNG,
	MPO,
	MCS,
	MCC,
	MEQ,
	MNE,
	MVS,
	MVC,
	OR ,
	MOV,
	IML,
	IDV,
	PSH,
	PUL,
	NEG,
	SET
};

enum base_isa {
	CPS_IMP = 0x00, /* Clear Processor Status. */
	ADC_IMM = 0x01, /* ADd with Carry. */
	ROR_IMM = 0x02, /* ROtate Right. */
	CPB_IMM = 0x04, /* ComPare B register. */
	ADC_Z   = 0x05, /* ADC Zero Matrix. */
	ROR_Z   = 0x06, /* ROR Zero Matrix. */
	CPB_Z   = 0x08, /* CPB Zero Matrix. */
	CLC_IMP = 0x09, /* CLear Carry flag. */
	TAB_IMP = 0x0A, /* Transfer Accumulator to B.  */
	STY_Z   = 0x0C, /* STore Y register. */
	JMP_AB  = 0x10, /* JMP Absolute. */
	ADC_AB  = 0x11, /* ADC Absolute. */
	ROR_AB  = 0x12, /* ROR Absolute. */
	CPB_AB  = 0x14, /* CPB Absolute. */
	ADC_B   = 0x15, /* ADC B Register. */
	ROR_B   = 0x16, /* ROR B Register. */
	STY_AB  = 0x18, /* STY Absolute. */
	SEC_IMP = 0x19, /* SEt Carry flag. */
	TBA_IMP = 0x1A, /* Transfer B to Accumulator.  */
	JMP_Z   = 0x20, /* JuMP to memory location. */
	SBC_IMM = 0x21, /* SuBtract with Carry. */
	MUL_IMM = 0x22, /* MULtiply accumulator. */
	CPX_IMM = 0x24, /* ComPare X register. */
	SBC_Z   = 0x25, /* SBC Zero Matrix. */
	MUL_Z   = 0x26, /* MUL Zero Matrix. */
	CPX_Z   = 0x28, /* CPX Zero Matrix. */
	CLI_IMP = 0x29, /* CLear Interupt flag. */
	TAY_IMP = 0x2A, /* Transfer Accumulator to Y. */
	STA_Z   = 0x2C, /* STore Accumulator. */
	STA_ZX  = 0x2E, /* STA Zero Marrix, Indexed with X. */
	JSR_AB  = 0x30, /* JSR Absolute. */
	SBC_AB  = 0x31, /* SBC Absolute. */
	MUL_AB  = 0x32, /* MUL Absolute. */
	CPX_AB  = 0x34, /* CPX Absolute. */
	SBC_B   = 0x35, /* SBC B Register. */
	MUL_B   = 0x36, /* MUL B Register. */
	STA_AB  = 0x38, /* STA Absolute. */
	SEI_IMP = 0x39, /* SEt Interupt flag. */
	TYA_IMP = 0x3A, /* Transfer Y to Accumulator. */
	STA_ZY  = 0x3C, /* STA Zero Marrix, Indexed with Y. */
	STA_IX  = 0x3E, /* STA Indexed Indirect. */
	JSR_Z   = 0x40, /* Jump to SubRoutine. */
	AND_IMM = 0x41, /* bitwise AND with accumulator. */
	DIV_IMM = 0x42, /* DIVide with accumulator. */
	CPY_IMM = 0x44, /* ComPare Y register. */
	AND_Z   = 0x45, /* AND Zero Matrix. */
	DIV_Z   = 0x46, /* DIV Zero Matrix. */
	CPY_Z   = 0x48, /* CPY Zero Matrix. */
	CLV_IMP = 0x49, /* CLear oVerflow flag. */
	TAX_IMP = 0x4A, /* Transfer Accumulator to X. */
	STB_Z   = 0x4C, /* STore B register. */
	STB_ZX  = 0x4E, /* STB Zero Marrix, Indexed with X. */
	RTS_IMP = 0x50, /* ReTurn from Subroutine. */
	AND_AB  = 0x51, /* AND Absolute. */
	DIV_AB  = 0x52, /* DIV Absolute. */
	CPY_AB  = 0x54, /* CPY Absolute. */
	AND_B   = 0x55, /* AND B Register. */
	DIV_B   = 0x56, /* DIV B Register. */
	STB_AB  = 0x58, /* STB Absolute. */
	WAI_IMP = 0x59, /* WAit for Interrupt. */
	TXA_IMP = 0x5A, /* Transfer X to Accumulator. */
	STB_ZY  = 0x5C, /* STB Zero Marrix, Indexed with Y. */
	STB_IX  = 0x5E, /* STB Indexed Indirect. */
	RTI_IMP = 0x60, /* ReTurn from Interrupt. */
	ORA_IMM = 0x61, /* bitwise OR with Accumulator. */
	ASR_IMM = 0x62, /* Arithmetic Shift Right. */
	LDX_IMM = 0x64, /* LoaD X register. */
	ORA_Z   = 0x65, /* ORA Zero Matrix. */
	ASR_Z   = 0x66, /* ASR Zero Matrix. */
	LDX_Z   = 0x68, /* LDX Zero Matrix. */
	BRK_IMP = 0x69, /* BReaK. */
	TYX_IMP = 0x6A, /* Transfer Y to X. */
	STX_Z   = 0x6C, /* STore X register. */
	PHP_IMP = 0x6E, /* PusH Processor status to stack. */
	BPO_REL = 0x70, /* Branch if POsitive. */
	ORA_AB  = 0x71, /* ORA Absolute. */
	ASR_AB  = 0x72, /* ASR Absolute. */
	LDX_AB  = 0x74, /* LDX Absolute. */
	ORA_B   = 0x75, /* ORA B Register. */
	ASR_B   = 0x76, /* ASR B Register. */
	STX_AB  = 0x78, /* STX Absolute. */
	DEY_IMP = 0x79, /* DEcrement Y register. */
	TXY_IMP = 0x7A, /* Transfer X to Y. */
	CPB_IN  = 0x7C, /* CPB Indirect */
	PLP_IMP = 0x7E, /* PuLl Processor status from stack. */
	BNG_REL = 0x80, /* Branch if NeGative. */
	XOR_IMM = 0x81, /* bitwise XOR with accumulator. */
	CMP_IMM = 0x82, /* CoMPare accumulator. */
	DEC_IMP = 0x84, /* DECrement accumulator. */
	XOR_Z   = 0x85, /* XOR Zero Matrix. */
	CMP_Z   = 0x86, /* CMP Zero Matrix. */
	DEC_Z   = 0x88, /* DEC Zero Matrix. */
	INY_IMP = 0x89, /* INcrement Y register. */
	TSX_IMP = 0x8A, /* Transfer Stack pointer to X. */
	CMP_IN  = 0x8C, /* CMP Indirect */
	PHA_IMP = 0x8E, /* PusH Accumulator to stack. */
	BCS_REL = 0x90, /* Branch if Carry Set. */
	XOR_AB  = 0x91, /* XOR Absolute. */
	CMP_AB  = 0x92, /* CMP Absolute. */
	DEC_AB  = 0x94, /* DEC Absolute. */
	XOR_B   = 0x95, /* XOR B Register. */
	CMP_B   = 0x96, /* CMP B Register. */
	DEB_IMP = 0x99, /* Decrement B register. */
	TXS_IMP = 0x9A, /* Transfer X to Stack pointer. */
	STY_IN  = 0x9C, /* STY Indirect */
	PLA_IMP = 0x9E, /* PuLl Accumulator from stack. */
	BCC_REL = 0xA0, /* Branch if Carry Clear. */
	LSL_IMM = 0xA1, /* Logical Shift Left. */
	LDY_IMM = 0xA2, /* LoaD Y register. */
	INC_IMP = 0xA4, /* INCrement accumulator. */
	LSL_Z   = 0xA5, /* LSL Zero Matrix. */
	LDY_Z   = 0xA6, /* LDY Zero Matrix. */
	INC_Z   = 0xA8, /* INC Zero Matrix. */
	INB_IMP = 0xA9, /* Increment B register. */
	CMP_IX  = 0xAA, /* CMP Indexed Indirect. */
	LDY_IN  = 0xAC, /* LDY Indirect */
	PHB_IMP = 0xAE, /* PusH B register to stack. */
	BEQ_REL = 0xB0, /* Branch if EQual. */
	LSL_AB  = 0xB1, /* LSL Absolute. */
	LDY_AB  = 0xB2, /* LDY Absolute. */
	INC_AB  = 0xB4, /* INC Absolute. */
	LSL_B   = 0xB5, /* LSL B Register. */
	DEX_IMP = 0xB9, /* DEcrement X register. */
	CPB_IX  = 0xBA, /* CPB Indexed Indirect. */
	LDX_IN  = 0xBC, /* LDX Indirect */
	PLB_IMP = 0xBE, /* PuLl B register to stack. */
	BNE_REL = 0xC0, /* Branch if Not Equal. */
	LSR_IMM = 0xC1, /* Logical Shift Right. */
	LDA_IMM = 0xC2, /* LoaD Accumulator. */
	LDA_IN  = 0xC4, /* LDA Indirect */
	LSR_Z   = 0xC5, /* LSR Zero Matrix. */
	LDA_Z   = 0xC6, /* LDA Zero Matrix. */
	LDA_ZX  = 0xC8, /* LDA Zero Marrix, Indexed with X. */
	INX_IMP = 0xC9, /* INcrement X register. */
	STA_IY  = 0xCA, /* STA Indirect Indexed. */
	STX_IN  = 0xCC, /* STX Indirect */
	PHY_IMP = 0xCE, /* PusH Y register to stack. */
	BVS_REL = 0xD0, /* Branch if oVerflow Set. */
	LSR_AB  = 0xD1, /* LSR Absolute. */
	LDA_AB  = 0xD2, /* LDA Absolute. */
	STA_IN  = 0xD4, /* STA Indirect */
	LSR_B   = 0xD5, /* LSR B Register. */
	LDA_ZY  = 0xD6, /* LDA Zero Marrix, Indexed with Y. */
	LDA_IX  = 0xD8, /* LDA Indexed Indirect. */
	LDA_IY  = 0xD9, /* LDA Indirect Indexed. */
	STB_IY  = 0xDA, /* STB Indirect Indexed. */
	JSR_IN  = 0xDC, /* JSR Indirect */
	PLY_IMP = 0xDE, /* PuLl Y register from stack. */
	BVC_REL = 0xE0, /* Branch if oVerflow Clear. */
	ROL_IMM = 0xE1, /* ROtate Left. */
	LDB_IMM = 0xE2, /* LoaD B register. */
	LDB_IN  = 0xE4, /* LDB Indirect */
	ROL_Z   = 0xE5, /* ROL Zero Matrix. */
	LDB_Z   = 0xE6, /* LDB Zero Matrix. */
	LDB_ZX  = 0xE8, /* LDB Zero Marrix, Indexed with X. */
	LDB_IY  = 0xE9, /* LDB Indirect Indexed. */
	NOP_IMP = 0xEA, /* No OPeration. */
	JMP_IN  = 0xEC, /* JMP Indirect */
	PHX_IMP = 0xEE, /* PusH X register to stack. */
	BRA_REL = 0xF0, /* BRanch Always. */
	ROL_AB  = 0xF1, /* ROL Absolute. */
	LDB_AB  = 0xF2, /* LDB Absolute. */
	STB_IN  = 0xF4, /* STB Indirect */
	ROL_B   = 0xF5, /* ROL B Register. */
	LDB_ZY  = 0xF6, /* LDB Zero Marrix, Indexed with Y. */
	LDB_IX  = 0xF8, /* LDB Indexed Indirect. */
	CMP_IY  = 0xF9, /* CMP Indirect Indexed. */
	CPB_IY  = 0xFA, /* CPB Indirect Indexed. */
	PLX_IMP = 0xFE  /* PuLl X register from stack. */
};

enum base_ext {
	LEA_AY  = 0x03, /* LEA Absolute, indexed with Y. */
	ADD_IMM = 0x06, /* ADD without carry. */
	LEA_Z   = 0x07, /* Load Effective Address. */
	CPE_IMM = 0x08, /* ComPare Effective address register. */
	CLZ_Z   = 0x09, /* Count Leading Zeros. */
	ADD_Z   = 0x0A, /* ADD Zero Matrix. */
	STB_E   = 0x0B, /* STB E Indirect. */
	CPE_Z   = 0x0C, /* CPE Zero Matrix. */
	LNG_IMM = 0x0D, /* Load accumulator, if NeGative. */
	LNG_E   = 0x0E, /* LNG E Indirect. */
	JMP_E   = 0x10, /* JMP E Indirect. */
	ADC_E   = 0x11, /* ADC E Indirect. */
	ROR_E   = 0x12, /* ROR E Indirect. */
	LEA_AB  = 0x13, /* LEA Absolute. */
	CLZ_AB  = 0x15, /* CLZ Absolute. */
	ADD_AB  = 0x16, /* ADD Absolute. */
	LEA_ZY  = 0x17, /* LEA Zero Matrix, indexed with Y. */
	CPE_AB  = 0x18, /* CPE Absolute. */
	CLZ_E   = 0x19, /* CLZ E Indirect. */
	ADD_E   = 0x1A, /* ADD E Indirect. */
	LDX_E   = 0x1B, /* LDX E Indirect. */
	SNG_E   = 0x1E, /* Store accumulator, if NeGative. */
	PEA_AY  = 0x23, /* PEA Absolute, indexed with Y. */
	SUB_IMM = 0x26, /* SUBtract without carry. */
	PEA_Z   = 0x27, /* Push Effective Address. */
	CLO_Z   = 0x29, /* Count Leading Ones. */
	SUB_Z   = 0x2A, /* SUB Zero Matrix. */
	STX_E   = 0x2B, /* STX E Indirect. */
	ICE_Z   = 0x2C, /* Interlocked Compare, and Exchange. */
	LPO_IMM = 0x2D, /* Load accumulator, if POsitive. */
	LPO_E   = 0x2E, /* LPO E Indirect. */
	JSR_E   = 0x30, /* JSR E Indirect. */
	SBC_E   = 0x31, /* SBC E Indirect. */
	MUL_E   = 0x32, /* MUL E Indirect. */
	PEA_AB  = 0x33, /* PEA Absolute. */
	CLO_AB  = 0x34, /* CLO Absolute. */
	SUB_AB  = 0x35, /* SUB Absolute. */
	PEA_ZY  = 0x37, /* PEA Zero Matrix, indexed with Y. */
	ICE_AB  = 0x38, /* ICE Absolute. */
	CLO_E   = 0x39, /* CLO E Indirect. */
	SUB_E   = 0x3A, /* SUB E Indirect. */
	CPB_E   = 0x3B, /* CPB E Indirect. */
	ICE_E   = 0x3C, /* ICE E Indirect. */
	SPO_E   = 0x3E, /* Store accumulator, if POsitive. */
	LDS_IMM = 0x40, /* LoaD Stack pointer. */
	LEA_AI  = 0x43, /* LEA Absolute Indirect. */
	LDS_Z   = 0x44, /* LDS Zero Matrix. */
	ADE_IMM = 0x46, /* ADd Effective address register. */
	LEA_IN  = 0x47, /* LEA Indirect. */
	BIT_Z   = 0x49, /* BIt Test. */
	ADE_Z   = 0x4A, /* ADE Zero Matrix. */
	CPX_E   = 0x4B, /* CPX E Indirect. */
	LLM_Z   = 0x4C, /* Logical shift Left, on Memory. */
	LCS_IMM = 0x4D, /* Load accumulator, if Carry Set. */
	LCS_E   = 0x4E, /* LCS E Indirect. */
	LDS_AB  = 0x50, /* LDS Absolute. */
	AND_E   = 0x51, /* AND E Indirect. */
	DIV_E   = 0x52, /* DIV E Indirect. */
	LEA_AX  = 0x53, /* LEA Absolute, indexed with X. */
	LDS_E   = 0x54, /* LDS E Indirect. */
	BIT_AB  = 0x55, /* BIT Absolute. */
	ADE_AB  = 0x56, /* ADE Absolute. */
	LEA_ZX  = 0x57, /* LEA Zero Matrix, indexed with X. */
	LLM_AB  = 0x58, /* LLM Absolute. */
	BIT_E   = 0x59, /* BIT E Indirect. */
	CPY_E   = 0x5B, /* CPY E Indirect. */
	LLM_E   = 0x5C, /* LLM E Indirect. */
	SCS_E   = 0x5E, /* Store accumulator, if Carry Set. */
	SCO_IMM = 0x60, /* Start one, or more COre(s). */
	PEA_AI  = 0x63, /* PEA Absolute Indirect. */
	SCO_Z   = 0x64, /* SCO Zero Matrix. */
	SBE_IMM = 0x66, /* SuBtract Effective address register. */
	PEA_IN  = 0x67, /* PEA Indirect. */
	SBE_Z   = 0x6A, /* SBE Zero Matrix. */
	PHE_IMP = 0x6B, /* PusH Effective address register to stack. */
	LRM_Z   = 0x6C, /* Logical shift Right, on Memory. */
	LCC_IMM = 0x6D, /* Load accumulator, if Carry Clear. */
	LCC_E   = 0x6E, /* LCC E Indirect. */
	SCO_AB  = 0x70, /* SCO Absolute. */
	ORA_E   = 0x71, /* ORA E Indirect. */
	ASR_E   = 0x72, /* ASR E Indirect. */
	PEA_AX  = 0x73, /* PEA Absolute, indexed with X. */
	SCO_E   = 0x74, /* SCO E Indirect. */
	SBE_AB  = 0x76, /* SBE Absolute. */
	PEA_ZX  = 0x77, /* PEA Zero Matrix, indexed with X. */
	LRM_AB  = 0x78, /* LRM Absolute. */
	PLE_IMP = 0x7B, /* PuLl Effective address register from stack. */
	LRM_E   = 0x7C, /* LRM E Indirect. */
	SCC_E   = 0x7E, /* Store accumulator, if Carry Clear. */
	ECO_IMM = 0x80, /* End one, or more COre(s). */
	DEC_E   = 0x82, /* DEC E Indirect. */
	LEA_AIY = 0x83, /* LEA Absolute Indirect Indexed. */
	ECO_Z   = 0x84, /* ECO Zero Matrix. */
	ADS_IMM = 0x86, /* ADd Stack pointer. */
	LEA_IY  = 0x87, /* LEA Indirect Indexed. */
	ADS_Z   = 0x8A, /* ADS Zero Matrix. */
	DEE_IMP = 0x8B, /* DEcrement Effective address register. */
	RLM_Z   = 0x8C, /* Rotate Left, on Memory. */
	LEQ_IMM = 0x8D, /* Load accumulator, if EQual. */
	LEQ_E   = 0x8E, /* LEQ E Indirect. */
	ECO_AB  = 0x90, /* ECO Absolute. */
	XOR_E   = 0x91, /* XOR E Indirect. */
	CMP_E   = 0x92, /* CMP E Indirect. */
	LEA_AIX = 0x93, /* LEA Absolute Indexed Indirect. */
	ECO_E   = 0x94, /* ECO E Indirect. */
	ADS_AB  = 0x96, /* ADS Absolute. */
	LEA_IX  = 0x97, /* LEA Indexed Indirect. */
	RLM_AB  = 0x98, /* RLM Absolute. */
	ADS_E   = 0x9A, /* ADS E Indirect. */
	INE_IMP = 0x9B, /* INcrement Effective address register. */
	RLM_E   = 0x9C, /* RLM E Indirect. */
	SEQ_E   = 0x9E, /* Store accumulator, if EQual. */
	INC_E   = 0xA2, /* INC E Indirect. */
	PEA_AIY = 0xA3, /* PEA Absolute Indirect Indexed. */
	STS_Z   = 0xA4, /* STore Stack pointer. */
	SBS_IMM = 0xA6, /* SuBtract Stack pointer. */
	PEA_IY  = 0xA7, /* PEA Indirect Indexed. */
	SBS_Z   = 0xAA, /* SBS Zero Matrix. */
	DES_IMP = 0xAB, /* DEcrement Stack pointer. */
	RRM_Z   = 0xAC, /* Rotate Right, on Memory. */
	LNE_IMM = 0xAD, /* Load accumulator, if Not Equal. */
	LNE_E   = 0xAE, /* LNE E Indirect. */
	STS_AB  = 0xB0, /* STS Absolute. */
	LSL_E   = 0xB1, /* LSL E Indirect. */
	LDY_E   = 0xB2, /* LDY E Indirect. */
	PEA_AIX = 0xB3, /* PEA Absolute Indexed Indirect. */
	STS_E   = 0xB4, /* STS E Indirect. */
	SBS_AB  = 0xB6, /* SBS Absolute. */
	PEA_IX  = 0xB7, /* PEA Indexed Indirect. */
	RRM_AB  = 0xB8, /* RRM Absolute. */
	SBS_E   = 0xBA, /* SBS E Indirect. */
	INS_IMP = 0xBB, /* INcrement Stack pointer. */
	RRM_E   = 0xBC, /* RRM E Indirect. */
	REP_REL = 0xBD, /* REPeat until counter is zero. */
	SNE_E   = 0xBE, /* Store accumulator, if Not Equal. */
	STY_E   = 0xC2, /* STY E Indirect. */
	STE_Z   = 0xC4, /* STore Effective address register. */
	NOT_A   = 0xC6, /* bitwise NOT with accumulator. */
	NOT_Z   = 0xCA, /* NOT Zero Matrix. */
	MMV_IMP = 0xCB, /* Memory MoVe. */
	ARM_Z   = 0xCC, /* Arithmetic shift Right, on Memory. */
	REQ_REL = 0xCD, /* Repeat until either counter is zero, or zero flag isn't set. */
	STE_AB  = 0xD0, /* STE Absolute. */
	LSR_E   = 0xD1, /* LSR E Indirect. */
	LDA_E   = 0xD2, /* LDA E Indirect. */
	NOT_AB  = 0xD6, /* NOT Absolute. */
	ARM_AB  = 0xD8, /* ARM Absolute. */
	NOT_E   = 0xDA, /* NOT E Indirect. */
	ARM_E   = 0xDC, /* ARM E Indirect. */
	RNE_REL = 0xDD, /* Repeat until either counter is zero, or zero flag is set. */
	STA_E   = 0xE2, /* STA E Indirect. */
	STZ_Z   = 0xE4, /* STore Zero. */
	SWP_A   = 0xE6, /* SWaP lower half, with upper half. */
	SWP_Z   = 0xEA, /* SWP Zero Matrix. */
	PCN_Z   = 0xEC, /* Population CouNt. */
	STZ_AB  = 0xF0, /* STZ Absolute. */
	ROL_E   = 0xF1, /* ROL E Indirect. */
	LDB_E   = 0xF2, /* LDB E Indirect. */
	STZ_E   = 0xF4, /* STZ E Indirect. */
	SWP_AB  = 0xF6, /* SWP Absolute. */
	PCN_AB  = 0xF8, /* PCN Absolute. */
	SWP_E   = 0xFA, /* SWP E Indirect. */
	PCN_E   = 0xFC  /* PCN E Indirect. */
};

#define ORTHO_1CC(mne, base, cc) \
	mne##_R##cc = base, mne##_M##cc = base|0x10

#define ORTHO_1OP(mne, base) \
	mne##_R   = base, mne##_M   = base|0x10

#define ORTHO_2OP(mne, base) \
	mne##_RR  = base, mne##_RM  = base|0x08, mne##_MR  = base|0x10, mne##_MM  = base|0x18

enum ortho {
	/* 0x00-0x1C */
	ORTHO_2OP(MNG, 0x00/**/),	/* Move if NeGative. */
	ORTHO_2OP(ADC, 0x01/**/),	/* ADC Ortho. */
	ORTHO_2OP(ROR, 0x02/**/),	/* ROR Ortho. */
	ORTHO_2OP(ADD, 0x03/**/),	/* ADD Ortho. */
	ORTHO_1OP(PSH, 0x04/**/),	/* PuSH operand onto the stack. */
	ORTHO_1CC(SET, 0x05, NG),	/* SET if NeGative. */
	ORTHO_1OP(JMP, 0x0C/**/),	/* JMP Ortho. */
	/* 0x20-0x3C */
	ORTHO_2OP(MPO, 0x20/**/),	/* Move if POsitive. */
	ORTHO_2OP(SBC, 0x21/**/),	/* SBC Ortho. */
	ORTHO_2OP(MUL, 0x22/**/),	/* MUL Ortho. */
	ORTHO_2OP(SUB, 0x23/**/),	/* SUB Ortho. */
	ORTHO_1OP(PUL, 0x24/**/),	/* PuLl operand off of the stack. */
	ORTHO_1CC(SET, 0x25, PO),	/* SET if POsitive. */
	ORTHO_1OP(JSR, 0x2C/**/),	/* JSR Ortho. */
	/* 0x40-0x5C */
	ORTHO_2OP(MCS, 0x40/**/),	/* Move if Carry Set. */
	ORTHO_2OP(AND, 0x41/**/),	/* AND Ortho. */
	ORTHO_2OP(DIV, 0x42/**/),	/* DIV Ortho. */
	ORTHO_2OP(PCN, 0x43/**/),	/* PCN Ortho. */
	ORTHO_1OP(NOT, 0x44/**/),	/* NOT Ortho. */
	ORTHO_1CC(SET, 0x45, CS),	/* SET if Carry Set. */
	ORTHO_1OP(PEA, 0x4C/**/),	/* PEA Ortho. */
	/* 0x60-0x7C */
	ORTHO_2OP(MCC, 0x60/**/),	/* Move if Carry Clear. */
	ORTHO_2OP(OR , 0x61/**/),	/* Bitwise OR. */
	ORTHO_2OP(ASR, 0x62/**/),	/* ASR Ortho. */
	ORTHO_2OP(LEA, 0x63/**/),	/* LEA Ortho. */
	ORTHO_1OP(NEG, 0x64/**/),	/* NEGate operand. */
	ORTHO_1CC(SET, 0x65, CC),	/* SET if Carry Clear. */
	ORTHO_1OP(SWP, 0x6C/**/),	/* SWP Ortho. */
	/* 0x80-0x95 */
	ORTHO_2OP(MEQ, 0x80/**/),	/* Move if EQual. */
	ORTHO_2OP(XOR, 0x81/**/),	/* XOR Ortho. */
	ORTHO_2OP(CMP, 0x82/**/),	/* CMP Ortho. */
	ORTHO_1OP(DEC, 0x84/**/),	/* DEC Ortho. */
	ORTHO_1CC(SET, 0x85, EQ),	/* SET if EQual. */
	/* 0xA0-0xB5 */
	ORTHO_2OP(MNE, 0xA0/**/),	/* Move if Not Equal. */
	ORTHO_2OP(LSL, 0xA1/**/),	/* LSL Ortho. */
	ORTHO_2OP(MOV, 0xA2/**/),	/* MOVe data from source, to destination. */
	ORTHO_1OP(INC, 0xA4/**/),	/* INC Ortho. */
	ORTHO_1CC(SET, 0xA5, NE),	/* SET if Not Equal. */
	/* 0xC0-0xD5 */
	ORTHO_2OP(MVS, 0xC0/**/),	/* Move if oVerflow Set. */
	ORTHO_2OP(LSR, 0xC1/**/),	/* LSR Ortho. */
	ORTHO_2OP(IML, 0xC2/**/),	/* Integer MuLtiply. */
	ORTHO_1OP(CLZ, 0xC4/**/),	/* CLZ Ortho. */
	ORTHO_1CC(SET, 0xC5, VS),	/* SET if oVerflow Set. */
	/* 0xE0-0xF5 */
	ORTHO_2OP(MVC, 0xE0/**/),	/* Move if oVerflow Clear. */
	ORTHO_2OP(ROL, 0xE1/**/),	/* ROL Ortho. */
	ORTHO_2OP(IDV, 0xE2/**/),	/* Integer DiVide. */
	ORTHO_1OP(CLO, 0xE4/**/),	/* CLO Ortho. */
	ORTHO_1CC(SET, 0xE5, VC)	/* SET if oVerflow Clear. */
};

#undef ORTHO_1CC
#undef ORTHO_1OP
#undef ORTHO_2OP
