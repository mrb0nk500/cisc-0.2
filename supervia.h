
struct shift_reg {
	union reg data;	/* Shift Register Data. */
	uint8_t bits;	/* Serial data for each byte. */
	uint8_t ocount;	/* Shift out counter. */
	uint8_t icount;	/* Shift out counter. */
	uint8_t dir;	/* Data direction for each byte. */
};


struct supervia {
	union reg pa;		/* Port A. */
	union reg pb;		/* Port B. */
	union reg ddra;		/* Port A Data Direction Register. */
	union reg ddrb;		/* Port B Data Direction Register. */
	union reg t[4];		/* Timers 1-4. */
	struct shift_reg sr[4];	/* Shift Registers A-D. */
	union reg acr;		/* Auxiliary Control Register. */
	union reg pcr;		/* Peripheral Control Register. */
	union reg ifr;		/* Interrupt Flags Register. */
	union reg ier;		/* Interrupt Enable Register. */
	union reg rng[2];	/* Random Number Generator A, and B. */
	union reg dac[2];	/* DAC A, and B. */
	union reg adc[2];	/* ADC A, and B. */
};
