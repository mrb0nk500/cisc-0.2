#include "subeditor.h"
#define clr_bitabl() {\
	bitabl[0x00] = 0;\
	bitabl[0x01] = 0;\
	bitabl[0x02] = 0;\
	bitabl[0x03] = 0;\
	bitabl[0x04] = 0;\
	bitabl[0x05] = 0;\
	bitabl[0x06] = 0;\
	bitabl[0x07] = 0;\
	bitabl[0x08] = 0;\
	bitabl[0x09] = 0;\
	bitabl[0x0A] = 0;\
	bitabl[0x0B] = 0;\
	bitabl[0x0C] = 0;\
	bitabl[0x0D] = 0;\
	bitabl[0x0E] = 0;\
	bitabl[0x0F] = 0;\
}

WINDOW *scr;

const char *tok = "dab";
const char *msg = "oof, you divided a, and b on me.\n";
const char *string = "Please, type something.\n";
const char *string2 = "You typed, ";

const uint8_t bits[8] = {
	0x80,
	0x40,
	0x20,
	0x10,
	0x08,
	0x04,
	0x02,
	0x01
};

char *buffer;
char *cmd_buf;

uint8_t scr_row = 0;
uint8_t scr_col = 0;
uint8_t scr_trow = 0;
uint8_t scr_tcol = 0;
uint16_t scr_ptr = 0;
uint16_t scr_ptr2 = 0;
uint16_t scr_ptr3 = 0;

uint8_t byte = 0;
uint8_t mask = 0;

uint8_t a = 0;
uint8_t b = 0;
uint8_t c = 0;
uint8_t d = 0;
uint8_t e = 0;
uint8_t f = 0;

uint8_t bitmask = 0;
uint8_t bitabl[16];

uint8_t scr_str = 0;
uint8_t scr_end = 23;
uint8_t wrapped = 0;

void clr_buf() {
	uint16_t i = 0;
	for (; i < 0x2000;) {
		buffer[i+0x00] = 0;
		buffer[i+0x01] = 0;
		buffer[i+0x02] = 0;
		buffer[i+0x03] = 0;
		buffer[i+0x04] = 0;
		buffer[i+0x05] = 0;
		buffer[i+0x06] = 0;
		buffer[i+0x07] = 0;
		buffer[i+0x08] = 0;
		buffer[i+0x09] = 0;
		buffer[i+0x0A] = 0;
		buffer[i+0x0B] = 0;
		buffer[i+0x0C] = 0;
		buffer[i+0x0D] = 0;
		buffer[i+0x0E] = 0;
		buffer[i+0x0F] = 0;
		buffer[i+0x10] = 0;
		buffer[i+0x11] = 0;
		buffer[i+0x12] = 0;
		buffer[i+0x13] = 0;
		buffer[i+0x14] = 0;
		buffer[i+0x15] = 0;
		buffer[i+0x16] = 0;
		buffer[i+0x17] = 0;
		buffer[i+0x18] = 0;
		buffer[i+0x19] = 0;
		buffer[i+0x1A] = 0;
		buffer[i+0x1B] = 0;
		buffer[i+0x1C] = 0;
		buffer[i+0x1D] = 0;
		buffer[i+0x1E] = 0;
		buffer[i+0x1F] = 0;
		i+=0x20;
	}
}

void clr_cbuf() {
	uint16_t i = 0;
	for (; i < 0x400;) {
		cmd_buf[i+0x00] = 0;
		cmd_buf[i+0x01] = 0;
		cmd_buf[i+0x02] = 0;
		cmd_buf[i+0x03] = 0;
		cmd_buf[i+0x04] = 0;
		cmd_buf[i+0x05] = 0;
		cmd_buf[i+0x06] = 0;
		cmd_buf[i+0x07] = 0;
		cmd_buf[i+0x08] = 0;
		cmd_buf[i+0x09] = 0;
		cmd_buf[i+0x0A] = 0;
		cmd_buf[i+0x0B] = 0;
		cmd_buf[i+0x0C] = 0;
		cmd_buf[i+0x0D] = 0;
		cmd_buf[i+0x0E] = 0;
		cmd_buf[i+0x0F] = 0;
		cmd_buf[i+0x10] = 0;
		cmd_buf[i+0x11] = 0;
		cmd_buf[i+0x12] = 0;
		cmd_buf[i+0x13] = 0;
		cmd_buf[i+0x14] = 0;
		cmd_buf[i+0x15] = 0;
		cmd_buf[i+0x16] = 0;
		cmd_buf[i+0x17] = 0;
		cmd_buf[i+0x18] = 0;
		cmd_buf[i+0x19] = 0;
		cmd_buf[i+0x1A] = 0;
		cmd_buf[i+0x1B] = 0;
		cmd_buf[i+0x1C] = 0;
		cmd_buf[i+0x1D] = 0;
		cmd_buf[i+0x1E] = 0;
		cmd_buf[i+0x1F] = 0;
		i+=0x20;
	}
}

#define dir(a) tokline[a].dir
#define mne(a) tokline[a].mne
#define amo(a) tokline[a].am
#define opb(a) tokline[a].opbase
#define com(a) tokline[a].com
#define lab(a) tokline[a].label
#define opc(a) tokline[a].op

#define tok(a) {		\
	dir(a) = 0xFF;		\
	mne(a) = 0xFF;		\
	amo(a) = 0xFF;		\
	opb(a) = 0xFF;		\
	com(a) = 0xFFFF;	\
	lab(a) = 0xFFFF;	\
	opc(a) = 0xFF;		\
}
void clr_tokline() {
	uint16_t i = 0;
	for (; i < 0x2000;) {
		tok(i+0x00);
		tok(i+0x01);
		tok(i+0x02);
		tok(i+0x03);
		tok(i+0x04);
		tok(i+0x05);
		tok(i+0x06);
		tok(i+0x07);
		tok(i+0x08);
		tok(i+0x09);
		tok(i+0x0A);
		tok(i+0x0B);
		tok(i+0x0C);
		tok(i+0x0D);
		tok(i+0x0E);
		tok(i+0x0F);
		tok(i+0x10);
		tok(i+0x11);
		tok(i+0x12);
		tok(i+0x13);
		tok(i+0x14);
		tok(i+0x15);
		tok(i+0x16);
		tok(i+0x17);
		tok(i+0x18);
		tok(i+0x19);
		tok(i+0x1A);
		tok(i+0x1B);
		tok(i+0x1C);
		tok(i+0x1D);
		tok(i+0x1E);
		tok(i+0x1F);
		i+=0x20;
	}
}

void update_pos() {
	scr_ptr = (scr_row+scr_str)*80;
	scr_ptr += scr_col;
	#if !debug
		wmove(scr, scr_row, scr_col);
	#endif
}

void rdrw_row() {
	uint8_t i = 0;
	uint16_t ptr;
	scr_col = 0;
	update_pos();
	ptr = scr_ptr;
	for (;i < 80;i++) {
		if (buffer[ptr+i]) {
			#if !debug
				waddch(scr, buffer[ptr+i]);
			#endif
		}
		scr_col++;
		if (!buffer[ptr+i]) {
			#if !debug
				waddch(scr, ' ');
			#endif
		}
	}
	scr_col = 0;
}

void rdrw_ln(uint8_t start, uint8_t end) {
	uint8_t tmp_row = scr_row;
	uint8_t tmp_col = scr_col;
	scr_row = start;
	for (; scr_row <= end; ) {
		rdrw_row();
		++scr_row;
	}
	scr_row = tmp_row;
	scr_col = tmp_col;
}

void scrl_down() {
	scr_str++;
	scr_end++;
	#if !debug
		wscrl(scr, 1);
	#endif
	rdrw_row();
	update_pos();
	wrapped = 0;
}

void scrl_up() {
	scr_str--;
	scr_end--;
	#if !debug
		wscrl(scr, -1);
	#endif
	scr_trow = scr_row;
	scr_tcol = scr_col;
	rdrw_row();
	scr_row = scr_trow;
	scr_col = scr_tcol;
	update_pos();
	wrapped = 0;
}

void bitpos(uint8_t row) {
	uint8_t bit;
	bitmask = row;
	bit = row & 7;
	mask = bits[bit];
	byte = bitmask >> 3;
}

void clrbit (uint8_t row) {
	uint8_t tmp;
	uint8_t invmsk;
	bitpos(row);
	invmsk = ~mask;
	tmp = (invmsk & (bitabl[byte]));
	bitabl[byte] = tmp;
}

void setbit (uint8_t row) {
	uint8_t tmp;
	bitpos(row);
	tmp = (mask | (bitabl[byte]));
	bitabl[byte] = tmp;
}

uint8_t getbit() {
	if (scr_str) {
		bitpos(scr_row+scr_str);
	} else {
		bitpos(scr_row);
	}
	return (mask & bitabl[byte]);
}

uint8_t findst() {
	uint8_t line_count = 0;
	while (getbit()) {
		line_count++;
		scr_row--;
		if ((int8_t)scr_row < 0) {
			line_count--;
			scr_row++;
			break;
		}
	}
	return line_count;
}

void fndend() {
	uint16_t i = scr_ptr;
	for (; buffer[i] != '\0'; i++);
	scr_ptr3 = i;
}

void findend() {
	fndend();
	c = (uint16_t)(scr_ptr3/80);
}


void shftln(uint16_t src, uint16_t dst, uint8_t flag) {
	if (!flag) {
		while (buffer[src] != '\0') {
			buffer[dst++] = buffer[src];
			buffer[src++] = '\0';
		}
		scr_ptr2 = scr_ptr;
		scr_ptr = src;
		findend();
		scr_ptr = scr_ptr2;
		if ((scr_ptr3 % 80 == 0) && (c > scr_row)) {
			clrbit(c);
		}
	} else {
		while (scr_ptr <= src) {
			if ((int16_t) src < 0) {
				src = 0;
				buffer[src] = '\0';
				break;
			}
			buffer[dst--] = buffer[src];
			buffer[src--] = '\0';
		}
		if (buffer[src+1] == '\0') {
			buffer[src+1] = ' ';
		}
		scr_ptr2 = scr_ptr;
		scr_ptr = src;
		findend();
		scr_ptr = scr_ptr2;
		if (buffer[src+1] == ' ') {
			buffer[src+1] = '\0';
		}
		if (c > scr_row) {
			setbit(c);
		}
	}
}

void print_char(char ch) {
	uint8_t is_wrap = 0;
	uint8_t is_scroll = 0;
	uint8_t is_esc = 0;
	uint8_t done = 0;
	uint16_t i = 0;
	uint16_t x = 0;
	uint16_t y = 0;
	a = ch;
	switch (ch) {
		case 0x1B:
			if (wgetch(scr) == 0x1B) {
				is_esc = 1;
			}
			ch = wgetch(scr);
			if (!ch)
				break;
			switch (ch) {
				case 'A':
					if (!scr_row) {
						if (scr_str) {
							scrl_up();
							update_pos();
							break;
						} else {
							break;
						}
					}
					scr_row--;
					update_pos();
					break;
				case 'B':
					if (scr_row == 23) {
						scr_trow = scr_row;
						scr_tcol = scr_col;
						scrl_down();
						scr_row = scr_trow;
						scr_col = scr_tcol;
						update_pos();
						break;
					}
					scr_row++;
					update_pos();
					break;
				case 'C':
					if (scr_col >= 79) {
						++scr_row;
						if (getbit()) {
							scr_col = 0;
							if (scr_row > 23) {
								wrapped = 1;
								scrl_down();
								is_wrap = 0;
								done = 1;
							} else {
								is_wrap = 1;
								done = 1;
							}
						} else {
							is_wrap = 0;
							done = 1;
						}
					} else {
						scr_col++;
						is_wrap = 1;
						done = 1;
					}
					if (!is_wrap) {
						--scr_row;
						is_wrap = 0;
					}
					if (done) {
						update_pos();
						done = 0;
					}
					wrapped = 0;
					break;
				case 'D':
					/* isleft. */
					if (!scr_col) {
						/* isleft_wrp. */
						if (getbit()) {
							/* wrap_dec. */
							if (scr_row) {
								wrapped = 1;
								scr_row--;
							}
							/* wrap_dec1. */
							scr_col = 79;
							if (!scr_row) {
								/* isleft_scrl. */
								if (wrapped) {
									done = 1;
								} else if (!scr_str) {
									done = 0;
								} else {
									scrl_up();
									done = 0;
								}
							} else {
								done = 1;
							}
						} else {
							done = 0;
						}
					} else {
						scr_col--;
						done = 1;
					}
					if (done) {
						update_pos();
						done = 0;
					}
					break;
			}
			break;
		case '\n':
			buffer[scr_ptr] = 0;
			scr_col = 0;
			if (scr_row == 23) {
				scrl_down();
			} else {
				scr_row++;
				update_pos();
			}
			a = '\n';
			wrefresh(scr);
			break;
		case 0x0C:
			scr_end = 23;
			scr_str = 0;
			clr_bitabl();
			clr_buf();
			clr_tokline();
			clr_cbuf();
			scr_row = 0;
			scr_col = 0;
			update_pos();
			werase(scr);
			break;
		case 19:
			break;
		case 18:
			break;
		case '\b':
		case 0x7F:
			if (!scr_col) {
				if (getbit()) {
					if (!scr_row) {
						if (scr_str) {
							scrl_up();
							scr_row++;
						} else {
							break;
						}
					}
					scr_row--;
					scr_col = 80;
					update_pos();
				} else {
					break;
				}
			}
			e = 0;
			scr_trow = scr_row;
			findend();
			scr_row = c;
			e = findst();
			buffer[--scr_ptr] = 0;
			shftln(scr_ptr+1, scr_ptr, 0);
			#if !debug
			int y, x;
			getyx(scr, y, x);
			if (x > 0) {
				wmove(scr, y, x-1);
			}
			wdelch(scr);
			#endif
			if (e) {
				findend();
				scr_tcol = scr_col;
				rdrw_ln(scr_row, c);
				scr_col = scr_tcol;
			}
			scr_row = scr_trow;
			scr_col--;
			update_pos();
			break;
		default:
			d = 0;
			if (buffer[scr_ptr] != '\0' && !b) {
				fndend();
				scr_ptr3++;
				shftln(scr_ptr3-2, scr_ptr3-1, 1);
				scr_trow = scr_row;
				scr_tcol = scr_col;
				a = ch;
				buffer[scr_ptr] = ch;
				findend();
				scr_row = c;
				findst();
				rdrw_ln(scr_row, c);
				scr_row = scr_trow;
				scr_col = scr_tcol;
				update_pos();
			} else {
				a = ch;
				buffer[scr_ptr] = ch;
			}
			scr_col++;
			#if !debug
				waddch(scr, ch);
			#endif
			if (scr_col >= 80) {
				if (scr_row >= 23) {
					is_scroll = 1;
					scrl_down();
				} else {
					is_scroll = 0;
					scr_row++;
				}
				scr_col = 0;
				setbit(scr_row+scr_str);
				update_pos();
			}
			update_pos();
			break;
	}
}

uint8_t getkey(char ch) {
	e = 0;
	b = 0;
	if (ch == '\n') {
		uint16_t i = 0;
		uint16_t ptr;
		scr_trow = scr_row;
		findend();
		scr_row = c;
		e = scr_row;
		findst();
		ptr = (scr_row+scr_str)*80;
		for (;buffer[ptr+i] != '\0';i++) {
			cmd_buf[i] = buffer[ptr+i];
		}
		if (e <= 23) {
			scr_row = e;
		} else {
			scr_row = 23;
		}
	}

	print_char(ch);
	wrefresh(scr);
	if (a == '\n') {
		return 0;
	} else {
		return 1;
	}
}

void print_str(const char *str) {
	uint16_t i = 0;
	b = 1;
	for (;str[i] != '\0'; i++) {
		print_char(str[i]);
	}
	b = 0;
}

uint8_t dabbed() {
	uint8_t i = 0;
	uint8_t done = 0;
	while (!done) {
		if (!cmd_buf[i]) {
			done = 1;
		} else if (cmd_buf[i] == tok[i]) {
			i++;
			if (i == 3) {
				print_str(msg);
				break;
			}
		} else {
			done = 1;
		}
	}
	return done;
}

int main() {
	if(!scr)
		scr = initscr();
	nodelay(stdscr, 0);
	crmode();
	noecho();
	nl();
	curs_set(1);
	werase(scr);
	scrollok(scr, 1);
	wrefresh(scr);
	start_color();
	use_default_colors();
	init_pair(1, COLOR_WHITE, -1);
	attron(COLOR_PAIR(1) | A_BOLD);
	wmove(scr, 0, 0);
	buffer = malloc(0x2000);
	cmd_buf = malloc(0x400);
	clr_bitabl();
	clr_buf();
	clr_tokline();
	uint8_t end = 0;
	uint8_t next = 1;
	char ch;
	while (!end) {
		if (next) {
			clr_cbuf();
			print_str(string);
			next = 0;
		}
		#if debug
		#if !debug_cmd_buf
		#define maxline 48
		#else
		#define maxline 24
		#endif
		uint8_t ln = 0;
		int row, col;
		mvwprintw(scr, ln++, 0, "scr_row: $%02X, scr_col: $%02X\r", scr_row, scr_col);
		mvwprintw(scr, ln++, 0, "scr_str: $%02X, scr_end: $%02X, scr_ptr: $%04X\r", scr_str, scr_end, scr_ptr);
		wmove(scr, ++ln, 0);
		wprintw(scr, "buffer:\r");
		wmove(scr, ++ln, 0);
		ln++;
		for (uint16_t i = 0; i < maxline; i++) {
			for (uint16_t j = 0; j < 80; j++) {
				wprintw(scr, "%02x", buffer[(j+(i*80))]);
				if (j == scr_col && i == scr_row) {
					getyx(scr, row, col);
					wmove(scr, ln++, 0);
					wclrtoeol(scr);
					wmove(scr, row+1, col-2);
					wprintw(scr, "/\\\r");
					wmove(scr, row, col);
					wrefresh(scr);
				}
			}
			wprintw(scr, ", i: $%02X\r", i);
			wmove(scr, ln++, 0);
		}
		#if debug_cmd_buf
		wprintw(scr, "cmd_buf:\r");
		wmove(scr, ln++, 0);
		for (uint16_t i = 0; i < 12; i++) {
			for (uint16_t j = 0; j < 80; j++) {
				wprintw(scr, "%02x", cmd_buf[(j+(i*80))]);
			}
			wprintw(scr, ", i: $%02X\r", i);
			wmove(scr, ln++, 0);
		}
		#endif
		wprintw(scr, "bitabl:\r");
		wmove(scr, ln++, 0);
		for (uint16_t i = 0; i < 0x10; i++) {
			wprintw(scr, "%02x", bitabl[i]);
		}
		waddch(scr, '\r');
		wrefresh(scr);
		#endif
		ch = wgetch(scr);
		if (ch == 0x11) {
			end = 1;
			continue;
		}
		if (!getkey(ch)) {
			if (subasm()) {
				print_str(string2);
				print_str(cmd_buf);
				print_char('\n');
			}
			next = 1;
		}
	}
	endwin();
	free(buffer);
	free(cmd_buf);
	return 0;
}
