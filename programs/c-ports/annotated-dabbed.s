dabbed:			; 472:	uint8_t dabbed() {
	ldb #0		;
	lda.w #tok	;
	sta.q ptr2	;
	tba		;
dab_st:			;
	phy #2		;
	txy		; 475:		while (!done) {
	lda (ptr6), y	; 476:			if (!cmd_buf[i]) {
	beq dab_pend	; 477:				done = 1;
	cmp (ptr2), y	; 478:			} else if (cmd_buf[i] == tok[i]) {
	beq chk_str	; 479:
	jmp dab_pend	; 484:			} else {
			; 485:				done = 1;
			; 486:			}
chk_str:		;
	ply #2		;
	inx		; 479:				i++;
	cpx #3		;
	bne dab_st	;
	ldx #0		; 480:				if (i == 3) {
pnt_msg:		;
	lda.w #msg	;
	ldx #0		;
	stx.q ptr	;
	jsl print_str	; 481:					print_str(msg);
	jmp dab_peqnd	; 482:					break;
			; 483:				}
dab_pend:		;
	ply #2		;
	lda #1		; 477/485:			done = 1;
	jmp dab_end	;
dab_peqnd:		;
	lda #0		;	 			done = 0;
	jmp dab_end	;
dab_end:		; 487:		}
	rtl		; 488:		return done;
			; 489:	}
