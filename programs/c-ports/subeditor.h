#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <curses.h>

#define debug 1
#define debug_cmd_buf 0

extern WINDOW *scr;

/* SuBEditor Stuff. */
extern char *buffer;
extern char *cmd_buf;

extern uint8_t scr_row;
extern uint8_t scr_col;
extern uint8_t scr_trow;
extern uint8_t scr_tcol;
extern uint16_t scr_ptr;

extern uint8_t byte;
extern uint8_t mask;

extern uint8_t a;
extern uint8_t b;
extern uint8_t c;
extern uint8_t d;
extern uint8_t e;
extern uint8_t f;

extern uint8_t bitmask;
extern uint8_t bitabl[];

extern uint8_t scr_str;
extern uint8_t scr_end;
extern uint8_t wrapped;

/* SuBAsm Stuff. */
struct line {
	uint8_t dir;
	uint8_t mne;
	uint8_t am;
	uint8_t opbase;
	uint16_t com;
	uint16_t label;
	uint64_t op;
};

char *label[0x1000];
char *comment[0x1000];
char *str[0x1000];
struct line tokline[0x1000];

extern void print_str(const char *str);
extern int str_cmp(const char *s0, const char *s1);
extern uint8_t subasm();
