.org 0

; Include Declarations.
.include "declare.s"
; Include SuBEditor.
.include "subeditor.s"
; Include SuBAsm.
.include "subasm.s"
; Include Lexer.
.include "lexer.s"
; Include Utility subroutines.
.include "utils.s"
; Include libc routines.
.include "libc.s"

.org $FFC0
.qword reset
a
;l a
;.org reset
;v
;f "subsuite.bin" $8000
;q
d
