
; Structs, and unions.

; Line struct.
.struct line
	next	.qword	; Pointer to next line.
	tok	.qword	; The tokens for this line.

	bline	.word	; Number of blank lines.
	lnum	.dword	; Line number.

	addr	.qword	; The address of this line.
.endstruct

; Token struct.
.struct tok
	;ptype	.byte	; Pointer type, 0 for token, -1 for end of token.
	next	.qword	; Pointer to next token.
	id	.byte	; Token ID.
	type	.byte	; Token type ID.

	tabs	.byte	; Number of tabs.
	spaces	.byte	; Number of spaces.
	stab	.byte	; Number of sub-token tabs.
	sspace	.byte	; Number of sub-token spaces.
	digits	.byte	; Number of digits.

	.union		; Token value.
		sym	.qword	; Symbol.
		str	.qword	; String.
		val	.qword	; Value.
	.endunion
.endstruct

; Fixup struct.
.struct fix
	next	.qword	; Pointer to next fixup.

	s	.qword	; Unresolved symbol.
	t	.qword	; Token that used the unresolved symbol.

	addr	.qword	; Address of where it happened.
.endstruct

; Symbol struct.
.struct sym
	next	.qword	; Pointer to next symbol.
	prev	.qword	; Pointer to previous symbol.
	down	.qword	; Pointer to child symbol.
	up	.qword	; Pointer to parent symbol.

	val	.qword	; Value of symbol.

	.union
		strct	.byte	; Bit 0: Struct/Union flag.
		anon	.byte	; Bit 1: Anonymous struct/union flag.
		def	.byte	; Bit 2: Defined symbol flag.
		flags	.byte	; Flags.
	.endunion

	name	.qword	; Name of symbol.
	id	.word	; ID of symbol.
.endstruct

; Instruction struct.
.struct instr
	am	.word	; Addressing modes.
	op	.byte	; Base value used to get the actual opcode.
.endstruct

; Free memory block struct, used by malloc.
.struct fblk
	size	.qword	; Size of free memory block.
	next	.qword	; Pointer to next free block.
	prev	.qword	; Pointer to previous free block.
.endstruct

; Used memory block struct, used by malloc.
.struct ublk
	size	.qword	; Size of used memory block.
	start	.qword	; Starting address of memory block.
.endstruct


; Enums.

; I/O constants.
status	= $100		; Keyboard status.
scr	= $101		; Character that is to be printed.
kbd	= $102		; Character from the Keyboard.
step	= $110		; Enables clock stepping, when set.

; Screen constants.
maxrow	= 23		; Screen's row count.
maxcol	= 79		; Screen's column count.

; Table sizes.
SCRSIZE	= $2000		; Screen buffer size, in bytes.
CMDSIZE	= $400		; Command buffer size, in bytes.
LWSIZE	= $1000		; Linewrap table size, in bytes.

; Magic number used by findramend.
MAGIC	= $AA

; Size of the Stack.
STKSIZE	= $30000

; Heap related values.
HEAPORG	= $30000	; Starting point of the heap.


; Directives.
DIR_ORG     =  0	; Origin.
DIR_BYTE    =  1	; Byte  =  8 bits.
DIR_WORD    =  2	; Word  = 16 bits.
DIR_DWORD   =  3	; Dword = 32 bits.
DIR_QWORD   =  4	; Qword = 64 bits.
DIR_INCL    =  5	; Include.
DIR_RES     =  6	; Reserved bytes.

; Tokens.
TOK_DIR     =  0	; Directive.
TOK_LOCAL   =  1	; Local syobol.
TOK_LABEL   =  2	; Label.
TOK_SYM     =  3	; Symbol.
TOK_EXPR    =  4	; Expression.
TOK_CSV     =  5	; Comma separated value.
TOK_STR     =  6	; String.
TOK_CHAR    =  7	; Character.
TOK_IND     =  8	; Indirect addressing.
TOK_IMM     =  9	; Immediate data.
TOK_BREG    = 10	; B register.
TOK_MNE     = 11	; Opcode/Mnemonic.
TOK_RS      = 12	; Register size prefix.
TOK_OF      = 13	; Offset register prefix.
TOK_COMM    = 14	; Comment.
TOK_HEX     = 15	; Hex value.
TOK_DEC     = 16	; Decimal value.
TOK_BIN     = 17	; Binary value.
TOK_INCL    = 18	; Include file.

; Pre-Tokens.
PTOK_DOT    =  0	; .
PTOK_AT     =  1	; @
PTOK_COLON  =  2	; :
PTOK_EQU    =  3	; =
PTOK_PLUS   =  4	; +
PTOK_MINUS  =  5	; -
PTOK_GT     =  6	; >
PTOK_LT     =  7	; <
PTOK_PIPE   =  8	; |
PTOK_LBRAK  =  9	; (
PTOK_RBRAK  = 10	; )
PTOK_COMMA  = 11	; ,
PTOK_B      = 12	; b
PTOK_X      = 13	; x
PTOK_Y      = 14	; y
PTOK_S      = 15	; s
PTOK_P      = 16	; p
PTOK_DQUOT  = 17	; "
PTOK_SQUOT  = 18	; '
PTOK_HASH   = 19	; #
PTOK_SCOLN  = 20	; ;
PTOK_DOLR   = 21	; $
PTOK_PRCNT  = 22	; %
PTOK_NUM    = 23	; 0-9
PTOK_ALPH   = 24	; a-z A-Z
PTOK_OTHR   = 25	; Everything else.

; Expressions.
EXPR_PLUS   =  0	; Plus.
EXPR_MINUS  =  1	; Minus.
EXPR_LOW    =  2	; Lower half of address.
EXPR_HIGH   =  3	; Upper half of address.
EXPR_NONE   =  4	; No expression.

; Addressing modes.
IMM   = 1 <<  0		; Immediate Data.
ZM    = 1 <<  1		; Zero Matrix.
ZMX   = 1 <<  2		; Zero Matrix, indexed with X.
ZMY   = 1 <<  3		; Zero Matrix, indexed with Y.
IND   = 1 <<  4		; Indirect.
INDX  = 1 <<  5		; Indexed Indirect.
INDY  = 1 <<  6		; Indirect Indexed.
ABS   = 1 <<  7		; Absolute.
REL   = 1 <<  8		; Relative.
BREG  = 1 <<  9		; B Register.
IMPL  = 1 << 10		; Implied.
INDX2 = 1 << 11		; Special case of INDX that uses the indirect table.
ZM2   = 1 << 12		; Special case of Zero Matrix used by JMP, and JSR.

OPNUM	= 74		; Instruction count.

; RAM declarations.

; Screen variables.
.org 0
scr_row:
	.res 1
scr_col:
	.res 1
scr_str:
	.res 1
scr_end:
	.res 1
scr_ptr:
	.res 2
scr_ptr2:
	.res 2
scr_ptr3:
	.res 2

; Pseudo registers.
rega:
	.res 1
regb:
	.res 1
regc:
	.res 1
regd:
	.res 1
rege:
	.res 1
regf:
	.res 1
regg:
	.res 1
; This pseudo register is always zero.
zero:
	.res 8
; End of pseudo registers.

end:
	.res 8
bitmask:
	.res 1
scr_trow:
	.res 1
scr_tcol:
	.res 1
wrapped:
	.res 1

; Pointers
ptr:
	.res 8
ptr2:
	.res 8
ptr3:
	.res 8


.org $20000

; Program Counter.
prg_cnt:
	.res 8
; Hex digit string buffer.
hex_str:
	.res 16
; String buffer.
strbuf:
	.res $80

; Subroutine pointer.
sub_ptr:
	.res 2

; Indecies.
idx0:
	.res 8
idx1:
	.res 8
idx2:
	.res 8
idx3:
	.res 8

; Value buffer used by strtoull.
valbuf:
	.res 8

; Copy buffer used by delmcpy.
cpybuf:
	.res 8

; Token ID, used by make_tok.
t_id:
	.res 1

; Token type, used by make_tok.
t_type:
	.res 1

; Number of spaces before a token, used by make_tok.
t_space:
	.res 1

; Number of tabs before a token, used by make_tok.
t_tab:
	.res 1

; Token value, used by make_tok.
t_val:
	.res 8

; Token string, used by make_tok.
t_str:
	.res 8

; Token symbol, used by make_tok.
t_sym:
	.res 8

; First token.
toks:
	.res 8

; Current token.
ctok:
	.res 8

; Last token.
ltok:
	.res 8

; First line.
lines:
	.res 8

; Current line.
cline:
	.res 8

; Last line.
lline:
	.res 8

; Lexeme type.
lex_type:
	.res 1

; Lexeme string.
lexeme:
	.res $100

; Size of the heap.
heapsize:
	.res 8

; Start of the heap.
heapstr:
	.res 8

; End of the heap.
heapend:
	.res 8

; Current top of the heap.
heapptr:
	.res 8

; First heap entry.
heapf:
	.res 8

; Last heap entry.
heapl:
	.res 8

; Linewrap table.
bitabl:
	.res 8

; Screen buffer.
buffer:
	.res 8

; Command buffer.
cmd_buf:
	.res 8

; Token table.
tokline:
	.res 8

; Symbol table.
symbol:
	.res 8

; Fixup table.
; Fixups are unresolved symbols.
fixup:
	.res 8

; Line number.
linenum:
	.res 4
ln:
	.res 4

; Is line number flag.
islinenum:
	.res 1

; ROM data declarations.

.org $C000
; String Literals/Constants.
;tok:
;	.byte "dab"
;msg:
;	.byte "oof, you divided a, and b on me.\n"

ed_name:
	.byte "SuBEditor"
ed_ver:
	.byte "1"
ed_sver:
	.byte ".0.0"

ver_str:
	.byte ", version "
made:
	.byte "Created by, "

author:
	.byte "mr b0nk 500"

;string2:
;	.byte "You typed, "

asm_name:
	.byte "SuBAsm"
asm_ver:
	.byte "0.1"

; Directives.
dir:
	.byte "org"
	.byte "byte"
	.byte "word"
	.byte "dword"
	.byte "qword"
	.byte "include"
	.byte "res"

; Short form Commands.
sh_cmds:
	.byte "vlahirs"

; Commands.
cmds:
	.byte "viewmem"
	.byte "list"
	.byte "asm"
	.byte "help"
	.byte "inst"
	.byte "run"
	.byte "set"

; Linewrap bitmask table.
bits:
	.byte $80, $40, $20, $10, $08, $04, $02, $01


; Instruction table.

; Format.
;
; 2 bytes: Addressing mode bits.
; 1 byte : Base value, used to generate the actual opcode.

inst:
	; ADC
	.word IMM|ZM|ABS|BREG
	.byte $01

	; AND
	.word IMM|ZM|ABS|BREG
	.byte $41

	; ASR
	.word IMM|ZM|ABS|BREG
	.byte $62

	; BCC
	.word REL
	.byte $A0

	; BCS
	.word REL
	.byte $90

	; BEQ
	.word REL
	.byte $B0

	; BNE
	.word REL
	.byte $C0

	; BNG
	.word REL
	.byte $80

	; BPO
	.word REL
	.byte $70

	; BRA
	.word REL
	.byte $F0

	; BRK
	.word IMPL
	.byte $69

	; BVC
	.word REL
	.byte $E0

	; BVS
	.word REL
	.byte $D0

	; CLC
	.word IMPL
	.byte $09

	; CLI
	.word IMPL
	.byte $29

	; CLV
	.word IMPL
	.byte $49

	; CMP
	.word IMM|ZM|IND|INDY|ABS|BREG|INDX2
	.byte $82

	; CPB
	.word IMM|ZM|IND|INDY|ABS|INDX2
	.byte $04

	; CPS
	.word IMPL
	.byte $00

	; CPX
	.word IMM|ZM|IND|ABS
	.byte $24

	; CPY
	.word IMM|ZM|IND|ABS
	.byte $44

	; DEB
	.word IMPL
	.byte $99

	; DEC
	.word IMPL|ZM|ABS
	.byte $84

	; DEX
	.word IMPL
	.byte $B9

	; DEY
	.word IMPL
	.byte $79

	; DIV
	.word IMM|ZM|ABS|BREG
	.byte $42

	; INB
	.word IMPL
	.byte $A9

	; INC
	.word IMPL|ZM|ABS
	.byte $A4

	; INX
	.word IMPL
	.byte $C9

	; INY
	.word IMPL
	.byte $89

	; JMP
	.word ABS|IND|ZM2
	.byte $00

	; JSR
	.word ABS|IND|ZM2
	.byte $20

	; LDA
	.word IMM|ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $C2

	; LDB
	.word IMM|ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $E2

	; LDX
	.word IMM|ZM|IND|ABS
	.byte $64

	; LDY
	.word IMM|ZM|IND|ABS
	.byte $A2

	; LSL
	.word IMM|ZM|ABS|BREG
	.byte $A1

	; LSR
	.word IMM|ZM|ABS|BREG
	.byte $C1

	; MUL
	.word IMM|ZM|ABS|BREG
	.byte $22

	; NOP
	.word IMPL
	.byte $EA

	; ORA
	.word IMM|ZM|ABS|BREG
	.byte $61

	; PHA
	.word IMPL
	.byte $8E

	; PHB
	.word IMPL
	.byte $AE

	; PHP
	.word IMPL
	.byte $6E

	; PHX
	.word IMPL
	.byte $EE

	; PHY
	.word IMPL
	.byte $CE

	; PLA
	.word IMPL
	.byte $9E

	; PLB
	.word IMPL
	.byte $BE

	; PLP
	.word IMPL
	.byte $7E

	; PLX
	.word IMPL
	.byte $FE

	; PLY
	.word IMPL
	.byte $DE

	; ROL
	.word IMM|ZM|ABS|BREG
	.byte $E1

	; ROR
	.word IMM|ZM|ABS|BREG
	.byte $02

	; RTI
	.word IMPL
	.byte $60

	; RTS
	.word IMPL
	.byte $50

	; SBC
	.word IMM|ZM|ABS|BREG
	.byte $21

	; SEC
	.word IMPL
	.byte $19

	; SEI
	.word IMPL
	.byte $39

	; STA
	.word ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $28

	; STB
	.word ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $48

	; STX
	.word ZM|IND|ABS
	.byte $68

	; STY
	.word ZM|IND|ABS
	.byte $08

	; TAB
	.word IMPL
	.byte $0A

	; TAX
	.word IMPL
	.byte $4A

	; TAY
	.word IMPL
	.byte $2A

	; TBA
	.word IMPL
	.byte $1A

	; TSX
	.word IMPL
	.byte $8A

	; TXA
	.word IMPL
	.byte $5A

	; TXS
	.word IMPL|IMM
	.byte $9A

	; TXY
	.word IMPL
	.byte $7A

	; TYA
	.word IMPL
	.byte $3A

	; TYX
	.word IMPL
	.byte $6A

	; WAI
	.word IMPL
	.byte $59

	; XOR
	.word IMM|ZM|ABS|BREG
	.byte $81

; Mnemonic Table.
mnem:
	.byte "ADC"
	.byte "AND"
	.byte "ASR"
	.byte "BCC"
	.byte "BCS"
	.byte "BEQ"
	.byte "BNE"
	.byte "BNG"
	.byte "BPO"
	.byte "BRA"
	.byte "BRK"
	.byte "BVC"
	.byte "BVS"
	.byte "CLC"
	.byte "CLI"
	.byte "CLV"
	.byte "CMP"
	.byte "CPB"
	.byte "CPS"
	.byte "CPX"
	.byte "CPY"
	.byte "DEB"
	.byte "DEC"
	.byte "DEX"
	.byte "DEY"
	.byte "DIV"
	.byte "INB"
	.byte "INC"
	.byte "INX"
	.byte "INY"
	.byte "JMP"
	.byte "JSR"
	.byte "LDA"
	.byte "LDB"
	.byte "LDX"
	.byte "LDY"
	.byte "LSL"
	.byte "LSR"
	.byte "MUL"
	.byte "NOP"
	.byte "ORA"
	.byte "PHA"
	.byte "PHB"
	.byte "PHP"
	.byte "PHX"
	.byte "PHY"
	.byte "PLA"
	.byte "PLB"
	.byte "PLP"
	.byte "PLX"
	.byte "PLY"
	.byte "ROL"
	.byte "ROR"
	.byte "RTI"
	.byte "RTS"
	.byte "SBC"
	.byte "SEC"
	.byte "SEI"
	.byte "STA"
	.byte "STB"
	.byte "STX"
	.byte "STY"
	.byte "TAB"
	.byte "TAX"
	.byte "TAY"
	.byte "TBA"
	.byte "TSX"
	.byte "TXA"
	.byte "TXS"
	.byte "TXY"
	.byte "TYA"
	.byte "TYX"
	.byte "WAI"
	.byte "XOR"


; Command subroutine table.
cmd_srt:
	.word viewmem
	.word list
	.word asm
	.word help
	.word instruction
	.word run
	.word set_val

; Return table used by get_ctrlidx.
ct_rtb:
	.byte 2
	.byte 0
	.byte 1
	.byte 0
	.byte 3
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 5
	.byte 4
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 0
	.byte 6

; Jump table for print_char's control codes.
ct_jtb:
	.word nl	; Newline.
	.word bs	; Backspace.
	.word clr_scr	; Ctrl+L.
	.word en_step	; Ctrl+S.
	.word dis_step	; Ctrl+R.
	.word esc	; Escape.
	.word printc	; Everything else (print it).

; Jump table for parsing pre-tokens.
swtab:
	.word ptok_dot	; PTOK_DOT
	.word ptok_at	; PTOK_AT
	.word ptok_col	; PTOK_COLON
	.word ptok_equ	; PTOK_EQU
	.word ptok_plus	; PTOK_PLUS
	.word ptok_min	; PTOK_MINUS
	.word ptok_gt	; PTOK_GT
	.word ptok_lt	; PTOK_LT
	.word ptok_pipe	; PTOK_PIPE
	.word ptok_lbrk	; PTOK_LBRAK
	.word ptok_rbrk	; PTOK_RBRAK
	.word ptok_com	; PTOK_COMMA
	.word ptok_br	; PTOK_B
	.word ptok_xr	; PTOK_X
	.word ptok_yr	; PTOK_Y
	.word ptok_sp	; PTOK_S
	.word ptok_pc	; PTOK_P
	.word ptok_dqu	; PTOK_DQUOT
	.word ptok_squ	; PTOK_SQUOT
	.word ptok_hash	; PTOK_HASH
	.word ptok_scol	; PTOK_SCOLN
	.word ptok_dolr	; PTOK_DOLR
	.word ptok_prcn	; PTOK_PRCNT
	.word ptok_num	; PTOK_NUM
	.word ptok_alph	; PTOK_ALPH
	.word ptok_othr	; PTOK_OTHR


; Hex character table.
hex_char:
	.byte "0123456789ABCDEF"

; Compare, and return table for pre-tokens.
ptok_tab:
	.byte ".@:=+-><|(),bxysp\"\'#;$%"
; Compare, and return table for isdelm.
dtab:
	.byte "\n,\"\' \\"
; Compare, and return table for isdelm2.
dtab2:
	.byte "),.+<|>-=;\n"
; Control code table for print_char.
ctrl_codes:
	.byte "\n\b\x0C\x13\x12\e"
