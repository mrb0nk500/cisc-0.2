void init_supervia(struct supervia *sv) {
	memset(sv, 0, sizeof(struct supervia));
}

/* Shifts out one bit from each output byte of the shift register into
 * the serial output lines of those output bytes.
 */
void shift_out8(struct supervia *sv, int id) {
	uint8_t bitpos = 7-sv->sr[id].ocount;
	for (int i = 0; i < 8; i++) {
		if (sv->sr[id].dir & (1 << i)) {
			uint8_t bit = (sv->sr[id].data.u8[i] >> bitpos) & 1;
			sv->sr[id].bits |= (bit << i);
		}
	}
	sv->sr[id].ocount++;
	sv->sr[id].ocount %= 8;
}
