#include "sux.h"
#include <assert.h>
#include <ctype.h>
#include <string.h>


void init_win(WINDOW *w) {
	init_screen();
	box(w, 0, 0);
	nodelay(w, 0);
	crmode();
	noecho();
	nl();
	keypad(w, 1);
	curs_set(1);
	werase(w);
	scrollok(w, 1);
	start_color();
	use_default_colors();
	init_pair(1, COLOR_WHITE, -1);
	attron(COLOR_PAIR(1) | A_BOLD);
	wmove(w, 0, 0);
}

int handle_char(int c) {

}

void emumon(WINDOW *w) {
	init_win(w);
	curs_set(0);
	wprintw(w, "Welcome to the debug monitor for CISC 0.2.\n");
	wprintw(w, "To get more info, type 'help'.\n");
	int done = 0;
	int row = 0;
	int col = 0;
	int insert = 1; /* Insert mode flag. */
	while (!done) {
		wmove(w, row, col);
		wrefresh(w);
		curs_set(1);
		int c = wgetch(w);
		curs_set(0);
		handle_char(c);
	}

}
