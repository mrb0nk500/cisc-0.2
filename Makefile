PREFIX := /usr/local
BIN_DIR := $(PREFIX)/bin

ifdef PCC
PCC_CFLAGS=-D__float128="long double"
else
PCC_CFLAGS=
endif

IO=1

ifdef BENCH
BENCH_CFLAGS=-Dbench=1
IO=
else
BENCH_CFLAGS=-Dbench=0
IO=1
endif

ifdef DEBUG
DBG_CFLAGS=-Ddebug=1
IO=
DBG_OBJ=disasm.o
else
ifdef IO
IO=1
endif
DBG_CFLAGS=-Ddebug=0
DBG_OBJ=
endif

ifdef IO
IO_CFLAGS=-DIO=1
else
IO_CFLAGS=-DIO=0
endif

ifdef GET_CLK
CLK_CFLAGS=-Dgetclk=1
else
CLK_CFLAGS=-Dgetclk=0
endif


#OBJS = sux.o io.o $(DBG_OBJ) asmmon.o assemble.o lexer.o microcode.o
OBJS = sux.o io.o $(DBG_OBJ) asmmon.o assemble.o lexer.o
OBJS2 = subasm.o subeditor.o
OBJS3 = opcode-gen.o csv-parse.o

CFLAGS = $(PCC_CFLAGS) $(DBG_CFLAGS) $(IO_CFLAGS) $(CLK_CFLAGS) $(BENCH_CFLAGS) $(CFLAGS_EXTRA)

OBJ_NAME = cisc-0.2
OBJ_NAME2 = subeditor-c
OBJ_NAME3 = opcode-gen

all : clean $(OBJ_NAME)

subeditor : clean $(OBJS2)
	$(CC) $(OBJS2) $(CFLAGS) -lcurses -ltinfo -o $(OBJ_NAME2)
cisc-0.2: $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) -lpthread -lcurses -ltinfo -o $(OBJ_NAME)
opcode-gen : clean $(OBJS3)
	$(CC) $(OBJS3) $(CFLAGS) -o $(OBJ_NAME3)
%.o : %.c
	$(CC) -c $< -o $@ $(CFLAGS)
clean :
	rm -f $(OBJ_NAME) $(OBJ_NAME2) $(OBJ_NAME3) *.o
install :
	install -D -m755 $(OBJ_NAME) $(BIN_DIR)/$(OBJ_NAME)
uninstall :
	rm -f $(BIN_DIR)/$(OBJ_NAME)
