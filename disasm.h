static const char *opname[0x100] = {
	[CPS_IMP ] = "CPS",
	[ADC_IMM ] = "ADC #",
	[ROR_IMM ] = "ROR #",
	[CPB_IMM ] = "CPB #",
	[ADC_Z   ] = "ADC zm",
	[ROR_Z   ] = "ROR zm",
	[CPB_Z   ] = "CPB zm",
	[CLC_IMP ] = "CLC",
	[TAB_IMP ] = "TAB",
	[STY_Z   ] = "STY zm",
	[JMP_AB  ] = "JMP a",
	[ADC_AB  ] = "ADC a",
	[ROR_AB  ] = "ROR a",
	[CPB_AB  ] = "CPB a",
	[ADC_B   ] = "ADC B",
	[ROR_B   ] = "ROR B",
	[STY_AB  ] = "STY a",
	[SEC_IMP ] = "SEC",
	[TBA_IMP ] = "TBA",
	[JMP_Z   ] = "JMP zm",
	[SBC_IMM ] = "SBC #",
	[MUL_IMM ] = "MUL #",
	[CPX_IMM ] = "CPX #",
	[SBC_Z   ] = "SBC zm",
	[MUL_Z   ] = "MUL zm",
	[CPX_Z   ] = "CPX zm",
	[CLI_IMP ] = "CLI",
	[TAY_IMP ] = "TAY",
	[STA_Z   ] = "STA zm",
	[STA_ZX  ] = "STA zmx",
	[JSR_AB  ] = "JSR a",
	[SBC_AB  ] = "SBC a",
	[MUL_AB  ] = "MUL a",
	[CPX_AB  ] = "CPX a",
	[SBC_B   ] = "SBC B",
	[MUL_B   ] = "MUL B",
	[STA_AB  ] = "STA a",
	[SEI_IMP ] = "SEI",
	[TYA_IMP ] = "TYA",
	[STA_ZY  ] = "STA zmy",
	[STA_IX  ] = "STA indx",
	[JSR_Z   ] = "JSR zm",
	[AND_IMM ] = "AND #",
	[DIV_IMM ] = "DIV #",
	[CPY_IMM ] = "CPY #",
	[AND_Z   ] = "AND zm",
	[DIV_Z   ] = "DIV zm",
	[CPY_Z   ] = "CPY zm",
	[CLV_IMP ] = "CLV",
	[TAX_IMP ] = "TAX",
	[STB_Z   ] = "STB zm",
	[STB_ZX  ] = "STB zmx",
	[RTS_IMP ] = "RTS",
	[AND_AB  ] = "AND a",
	[DIV_AB  ] = "DIV a",
	[CPY_AB  ] = "CPY a",
	[AND_B   ] = "AND B",
	[DIV_B   ] = "DIV B",
	[STB_AB  ] = "STB a",
	[WAI_IMP ] = "WAI",
	[TXA_IMP ] = "TXA",
	[STB_ZY  ] = "STB zmy",
	[STB_IX  ] = "STB indx",
	[RTI_IMP ] = "RTI",
	[ORA_IMM ] = "ORA #",
	[ASR_IMM ] = "ASR #",
	[LDX_IMM ] = "LDX #",
	[ORA_Z   ] = "ORA zm",
	[ASR_Z   ] = "ASR zm",
	[LDX_Z   ] = "LDX zm",
	[BRK_IMP ] = "BRK",
	[TYX_IMP ] = "TYX",
	[STX_Z   ] = "STX zm",
	[PHP_IMP ] = "PHP",
	[BPO_REL ] = "BPO rel",
	[ORA_AB  ] = "ORA a",
	[ASR_AB  ] = "ASR a",
	[LDX_AB  ] = "LDX a",
	[ORA_B   ] = "ORA B",
	[ASR_B   ] = "ASR B",
	[STX_AB  ] = "STX a",
	[DEY_IMP ] = "DEY",
	[TXY_IMP ] = "TXY",
	[CPB_IN  ] = "CPB ind",
	[PLP_IMP ] = "PLP",
	[BNG_REL ] = "BNG rel",
	[XOR_IMM ] = "XOR #",
	[CMP_IMM ] = "CMP #",
	[DEC_IMP ] = "DEC",
	[XOR_Z   ] = "XOR zm",
	[CMP_Z   ] = "CMP zm",
	[DEC_Z   ] = "DEC zm",
	[INY_IMP ] = "INY",
	[TSX_IMP ] = "TSX",
	[CMP_IN  ] = "CMP ind",
	[PHA_IMP ] = "PHA",
	[BCS_REL ] = "BCS rel",
	[XOR_AB  ] = "XOR a",
	[CMP_AB  ] = "CMP a",
	[DEC_AB  ] = "DEC a",
	[XOR_B   ] = "XOR B",
	[CMP_B   ] = "CMP B",
	[DEB_IMP ] = "DEB",
	[TXS_IMP ] = "TXS",
	[STY_IN  ] = "STY ind",
	[PLA_IMP ] = "PLA",
	[BCC_REL ] = "BCC rel",
	[LSL_IMM ] = "LSL #",
	[LDY_IMM ] = "LDY #",
	[INC_IMP ] = "INC",
	[LSL_Z   ] = "LSL zm",
	[LDY_Z   ] = "LDY zm",
	[INC_Z   ] = "INC zm",
	[INB_IMP ] = "INB",
	[CMP_IX  ] = "CMP indx",
	[LDY_IN  ] = "LDY ind",
	[PHB_IMP ] = "PHB",
	[BEQ_REL ] = "BEQ rel",
	[LSL_AB  ] = "LSL a",
	[LDY_AB  ] = "LDY a",
	[INC_AB  ] = "INC a",
	[LSL_B   ] = "LSL B",
	[DEX_IMP ] = "DEX",
	[CPB_IX  ] = "CPB indx",
	[LDX_IN  ] = "LDX ind",
	[PLB_IMP ] = "PLB",
	[BNE_REL ] = "BNE rel",
	[LSR_IMM ] = "LSR #",
	[LDA_IMM ] = "LDA #",
	[LDA_IN  ] = "LDA ind",
	[LSR_Z   ] = "LSR zm",
	[LDA_Z   ] = "LDA zm",
	[LDA_ZX  ] = "LDA zmx",
	[INX_IMP ] = "INX",
	[STA_IY  ] = "STA indy",
	[STX_IN  ] = "STX ind",
	[PHY_IMP ] = "PHY",
	[BVS_REL ] = "BVS rel",
	[LSR_AB  ] = "LSR a",
	[LDA_AB  ] = "LDA a",
	[STA_IN  ] = "STA ind",
	[LSR_B   ] = "LSR B",
	[LDA_ZY  ] = "LDA zmy",
	[LDA_IX  ] = "LDA indx",
	[LDA_IY  ] = "LDA indy",
	[STB_IY  ] = "STB indy",
	[JSR_IN  ] = "JSR ind",
	[PLY_IMP ] = "PLY",
	[BVC_REL ] = "BVC rel",
	[ROL_IMM ] = "ROL #",
	[LDB_IMM ] = "LDB #",
	[LDB_IN  ] = "LDB ind",
	[ROL_Z   ] = "ROL zm",
	[LDB_Z   ] = "LDB zm",
	[LDB_ZX  ] = "LDB zmx",
	[LDB_IY  ] = "LDB indy",
	[NOP_IMP ] = "NOP",
	[JMP_IN  ] = "JMP ind",
	[PHX_IMP ] = "PHX",
	[BRA_REL ] = "BRA rel",
	[ROL_AB  ] = "ROL a",
	[LDB_AB  ] = "LDB a",
	[STB_IN  ] = "STB ind",
	[ROL_B   ] = "ROL B",
	[LDB_ZY  ] = "LDB zmy",
	[LDB_IX  ] = "LDB indx",
	[CMP_IY  ] = "CMP indy",
	[CPB_IY  ] = "CPB indy",
	[PLX_IMP ] = "PLX"
};

static const char *ext_opname[0x100] = {
	[LEA_AY ] = "LEA a, y",
	[ADD_IMM] = "ADD #",
	[LEA_Z  ] = "LEA zm",
	[CPE_IMM] = "CPE #",
	[CLZ_Z  ] = "CLZ zm",
	[ADD_Z  ] = "ADD zm",
	[STB_E  ] = "STB (E)",
	[CPE_Z  ] = "CPE zm",
	[LNG_IMM] = "LNG #",
	[LNG_E  ] = "LNG (E)",
	[JMP_E  ] = "JMP (E)",
	[ADC_E  ] = "ADC (E)",
	[ROR_E  ] = "ROR (E)",
	[LEA_AB ] = "LEA a",
	[CLZ_AB ] = "CLZ a",
	[ADD_AB ] = "ADD a",
	[LEA_ZY ] = "LEA zm, y",
	[CPE_AB ] = "CPE a",
	[CLZ_E  ] = "CLZ (E)",
	[ADD_E  ] = "ADD (E)",
	[LDX_E  ] = "LDX (E)",
	[SNG_E  ] = "SNG (E)",
	[PEA_AY ] = "PEA a, y",
	[SUB_IMM] = "SUB #",
	[PEA_Z  ] = "PEA zm",
	[CLO_Z  ] = "CLO zm",
	[SUB_Z  ] = "SUB zm",
	[STX_E  ] = "STX (E)",
	[ICE_Z  ] = "ICE (E)",
	[LPO_IMM] = "LPO #",
	[LPO_E  ] = "LPO (E)",
	[JSR_E  ] = "JSR (E)",
	[SBC_E  ] = "SBC (E)",
	[MUL_E  ] = "MUL (E)",
	[PEA_AB ] = "PEA a",
	[CLO_AB ] = "CLO a",
	[SUB_AB ] = "SUB a",
	[PEA_ZY ] = "PEA zm, y",
	[ICE_AB ] = "ICE a",
	[CLO_E  ] = "CLO (E)",
	[SUB_E  ] = "SUB (E)",
	[CPB_E  ] = "CPB (E)",
	[ICE_E  ] = "ICE (E)",
	[SPO_E  ] = "SPO (E)",
	[LDS_IMM] = "LDS #",
	[LEA_AI ] = "LEA (a)",
	[LDS_Z  ] = "LDS zm",
	[ADE_IMM] = "ADE #",
	[LEA_IN ] = "LEA (zm)",
	[BIT_Z  ] = "BIT zm",
	[ADE_Z  ] = "ADE zm",
	[CPX_E  ] = "CPX (E)",
	[LLM_Z  ] = "LLM zm",
	[LCS_IMM] = "LCS #",
	[LCS_E  ] = "LCS (E)",
	[LDS_AB ] = "LDS a",
	[AND_E  ] = "AND (E)",
	[DIV_E  ] = "DIV (E)",
	[LEA_AX ] = "LEA a, x",
	[LDS_E  ] = "LDS (E)",
	[BIT_AB ] = "BIT a",
	[ADE_AB ] = "ADE a",
	[LEA_ZX ] = "LEA zm, x",
	[LLM_AB ] = "LLM a",
	[BIT_E  ] = "BIT (E)",
	[CPY_E  ] = "CPY (E)",
	[LLM_E  ] = "LLM (E)",
	[SCS_E  ] = "SCS (E)",
	[SCO_IMM] = "SCO #",
	[PEA_AI ] = "PEA (a)",
	[SCO_Z  ] = "SCO zm",
	[SBE_IMM] = "SBE #",
	[PEA_IN ] = "PEA (zm)",
	[SBE_Z  ] = "SBE zm",
	[PHE_IMP] = "PHE",
	[LRM_Z  ] = "LRM zm",
	[LCC_IMM] = "LCC #",
	[LCC_E  ] = "LCC (E)",
	[SCO_AB ] = "SCO a",
	[ORA_E  ] = "ORA (E)",
	[ASR_E  ] = "ASR (E)",
	[PEA_AX ] = "PEA a, x",
	[SCO_E  ] = "SCO (E)",
	[SBE_AB ] = "SBE a",
	[PEA_ZX ] = "PEA zm, x",
	[LRM_AB ] = "LRM a",
	[PLE_IMP] = "PLE",
	[LRM_E  ] = "LRM (E)",
	[SCC_E  ] = "SCC (E)",
	[ECO_IMM] = "ECO #",
	[DEC_E  ] = "DEC (E)",
	[LEA_AIY] = "LEA (a), y",
	[ECO_Z  ] = "ECO zm",
	[ADS_IMM] = "ADS #",
	[LEA_IY ] = "LEA (zm), y",
	[ADS_Z  ] = "ADS zm",
	[DEE_IMP] = "DEE",
	[RLM_Z  ] = "RLM zm",
	[LEQ_IMM] = "LEQ #",
	[LEQ_E  ] = "LEQ (E)",
	[ECO_AB ] = "ECO a",
	[XOR_E  ] = "XOR (E)",
	[CMP_E  ] = "CMP (E)",
	[LEA_AIX] = "LEA (a, x)",
	[ECO_E  ] = "ECO (E)",
	[ADS_AB ] = "ADS a",
	[LEA_IX ] = "LEA (zm, x)",
	[RLM_AB ] = "RLM a",
	[ADS_E  ] = "ADS (E)",
	[INE_IMP] = "INE",
	[RLM_E  ] = "RLM (E)",
	[SEQ_E  ] = "SEQ (E)",
	[INC_E  ] = "INC (E)",
	[PEA_AIY] = "PEA (a), y",
	[STS_Z  ] = "STS zm",
	[SBS_IMM] = "SBS #",
	[PEA_IY ] = "PEA (zm), y",
	[SBS_Z  ] = "SBS zm",
	[DES_IMP] = "DES",
	[RRM_Z  ] = "RRM zm",
	[LNE_IMM] = "LNE #",
	[LNE_E  ] = "LNE (E)",
	[STS_AB ] = "STS a",
	[LSL_E  ] = "LSL (E)",
	[LDY_E  ] = "LDY (E)",
	[PEA_AIX] = "PEA (a, x)",
	[STS_E  ] = "STS (E)",
	[SBS_AB ] = "SBS a",
	[PEA_IX ] = "PEA (zm, x)",
	[RRM_AB ] = "RRM a",
	[SBS_E  ] = "SBS (E)",
	[INS_IMP] = "INS",
	[RRM_E  ] = "RRM (E)",
	[REP_REL] = "REP rel",
	[SNE_E  ] = "SNE (E)",
	[STY_E  ] = "STY (E)",
	[STE_Z  ] = "STE zm",
	[NOT_A  ] = "NOT A",
	[NOT_Z  ] = "NOT zm",
	[MMV_IMP] = "MMV",
	[ARM_Z  ] = "ARM zm",
	[REQ_REL] = "REQ rel",
	[STE_AB ] = "STE a",
	[LSR_E  ] = "LSR (E)",
	[LDA_E  ] = "LDA (E)",
	[NOT_AB ] = "NOT a",
	[ARM_AB ] = "ARM a",
	[NOT_E  ] = "NOT (E)",
	[ARM_E  ] = "ARM (E)",
	[RNE_REL] = "RNE rel",
	[STA_E  ] = "STA (E)",
	[STZ_Z  ] = "STZ zm",
	[SWP_A  ] = "SWP A",
	[SWP_Z  ] = "SWP zm",
	[PCN_Z  ] = "PCN zm",
	[STZ_AB ] = "STZ a",
	[ROL_E  ] = "ROL (E)",
	[LDB_E  ] = "LDB (E)",
	[STZ_E  ] = "STZ (E)",
	[SWP_AB ] = "SWP a",
	[PCN_AB ] = "PCN a",
	[SWP_E  ] = "SWP (E)",
	[PCN_E  ] = "PCN (E)"
};

static const char *set_cc[8] = {
	"NG",
	"PO",
	"CS",
	"CC",
	"EQ",
	"NE",
	"VS",
	"VC"
};

#define ORTHO_1CC(mne, cc) \
	[mne##_R##cc] = #mne " r, " #cc, [mne##_M##cc] = #mne " m, " #cc

#define ORTHO_1OP(mne) \
	[mne##_R] = #mne " r", [mne##_M] = #mne " m"

#define ORTHO_2OP(mne) \
	[mne##_RR] = #mne " r, r", [mne##_RM] = #mne " r, m", [mne##_MR] = #mne " m, r", [mne##_MM] = #mne " m, m"


static const char *ortho_opname[] = {
	/* 0x00-0x1C */
	ORTHO_2OP(MNG/**/),	/* Move if NeGative. */
	ORTHO_2OP(ADC/**/),	/* ADC Ortho. */
	ORTHO_2OP(ROR/**/),	/* ROR Ortho. */
	ORTHO_2OP(ADD/**/),	/* ADD Ortho. */
	ORTHO_1OP(PSH/**/),	/* PuSH operand onto the stack. */
	ORTHO_1CC(SET, NG),	/* SET if NeGative. */
	ORTHO_1OP(JMP/**/),	/* JMP Ortho. */
	/* 0x20-0x3C */
	ORTHO_2OP(MPO/**/),	/* Move if POsitive. */
	ORTHO_2OP(SBC/**/),	/* SBC Ortho. */
	ORTHO_2OP(MUL/**/),	/* MUL Ortho. */
	ORTHO_2OP(SUB/**/),	/* SUB Ortho. */
	ORTHO_1OP(PUL/**/),	/* PuLl operand off of the stack. */
	ORTHO_1CC(SET, PO),	/* SET if POsitive. */
	ORTHO_1OP(JSR/**/),	/* JSR Ortho. */
	/* 0x40-0x5C */
	ORTHO_2OP(MCS/**/),	/* Move if Carry Set. */
	ORTHO_2OP(AND/**/),	/* AND Ortho. */
	ORTHO_2OP(DIV/**/),	/* DIV Ortho. */
	ORTHO_2OP(PCN/**/),	/* PCN Ortho. */
	ORTHO_1OP(NOT/**/),	/* NOT Ortho. */
	ORTHO_1CC(SET, CS),	/* SET if Carry Set. */
	ORTHO_1OP(PEA/**/),	/* PEA Ortho. */
	/* 0x60-0x7C */
	ORTHO_2OP(MCC/**/),	/* Move if Carry Clear. */
	ORTHO_2OP(OR /**/),	/* Bitwise OR. */
	ORTHO_2OP(ASR/**/),	/* ASR Ortho. */
	ORTHO_2OP(LEA/**/),	/* LEA Ortho. */
	ORTHO_1OP(NEG/**/),	/* NEGate operand. */
	ORTHO_1CC(SET, CC),	/* SET if Carry Clear. */
	ORTHO_1OP(SWP/**/),	/* SWP Ortho. */
	/* 0x80-0x95 */
	ORTHO_2OP(MEQ/**/),	/* Move if EQual. */
	ORTHO_2OP(XOR/**/),	/* XOR Ortho. */
	ORTHO_2OP(CMP/**/),	/* CMP Ortho. */
	ORTHO_1OP(DEC/**/),	/* DEC Ortho. */
	ORTHO_1CC(SET, EQ),	/* SET if EQual. */
	/* 0xA0-0xB5 */
	ORTHO_2OP(MNE/**/),	/* Move if Not Equal. */
	ORTHO_2OP(LSL/**/),	/* LSL Ortho. */
	ORTHO_2OP(MOV/**/),	/* MOVe data from source, to destination. */
	ORTHO_1OP(INC/**/),	/* INC Ortho. */
	ORTHO_1CC(SET, NE),	/* SET if Not Equal. */
	/* 0xC0-0xD5 */
	ORTHO_2OP(MVS/**/),	/* Move if oVerflow Set. */
	ORTHO_2OP(LSR/**/),	/* LSR Ortho. */
	ORTHO_2OP(IML/**/),	/* Integer MuLtiply. */
	ORTHO_1OP(CLZ/**/),	/* CLZ Ortho. */
	ORTHO_1CC(SET, VS),	/* SET if oVerflow Set. */
	/* 0xE0-0xF5 */
	ORTHO_2OP(MVC/**/),	/* Move if oVerflow Clear. */
	ORTHO_2OP(ROL/**/),	/* ROL Ortho. */
	ORTHO_2OP(IDV/**/),	/* Integer DiVide. */
	ORTHO_1OP(CLO/**/),	/* CLO Ortho. */
	ORTHO_1CC(SET, VC)	/* SET if oVerflow Clear. */
};

#undef ORTHO_1CC
#undef ORTHO_1OP
#undef ORTHO_2OP
