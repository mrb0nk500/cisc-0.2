static uint8_t isdelm(char c, uint8_t dbg) {
	switch (c) {
		default  : return 0x00;
		case '\0':
		case '\n': return 0x01;
		case ',' : return 0x02;
		case '\"': return 0x04;
		case '\'': return 0x08;
		case '\t':
		case ' ' : return 0x10;
	}
}

static uint8_t isdelm2(char c, uint8_t dbg) {
	switch (c) {
		default  : return 0;
		case ')' :
		case ',' :
		case '.' :
		case '+' :
		case '*' :
		case '<' :
		case '|' :
		case '>' :
		case '-' :
		case ':' :
		case '=' :
		case ';' :
		case '\0':
		case '\n': return 1;
		case '\t':
		case ' ' : return 2;
	}
}

static uint8_t get_ptok(char c, uint8_t dbg) {
	switch (c) {
		  case '.' : return PTOK_DOT	;
		  case '@' : return PTOK_AT	;
		  case ':' : return PTOK_COLON	;
		  case '=' : return PTOK_EQU	;
		  case '+' : return PTOK_PLUS	;
		  case '-' : return PTOK_MINUS	;
		  case '*' : return PTOK_ASTRSK	;
		  case '>' : return PTOK_GT	;
		  case '<' : return PTOK_LT	;
		  case '|' : return PTOK_PIPE	;
		  case '(' : return PTOK_LBRACK	;
		  case ')' : return PTOK_RBRACK	;
		  case ',' : return PTOK_COMMA	;
	case 'B': case 'b' : return PTOK_B	;
	case 'E': case 'e' : return PTOK_E	;
	case 'X': case 'x' : return PTOK_X	;
	case 'Y': case 'y' : return PTOK_Y	;
	case 'S': case 's' : return PTOK_S	;
	case 'P': case 'p' : return PTOK_P	;
	case 'A': case 'a' : return PTOK_A	;
	case 'C': case 'c' : return PTOK_C	;
	case 'D': case 'd' : return PTOK_D	;
	case 'F': case 'f' : return PTOK_F	;
	case 'R': case 'r' : return PTOK_R	;
		  case '\"': return PTOK_DQUOTE	;
		  case '\'': return PTOK_SQUOTE	;
		  case '#' : return PTOK_HASH	;
		  case ';' : return PTOK_SCOLON	;
		  case '$' : return PTOK_DOLLAR	;
		  case '%' : return PTOK_PERCENT;
		  default  :
			if (isdigit(c)) {
				return PTOK_NUMBER;
			} else if (isalpha(c) || c == '_') {
				return PTOK_ALPHA;
			} else {
				return PTOK_OTHER;
			}
	}
}

static uint8_t is_altok(uint8_t ptok, uint8_t dbg) {
	switch (ptok) {
		case PTOK_B:
		case PTOK_E:
		case PTOK_X:
		case PTOK_Y:
		case PTOK_S:
		case PTOK_P:
		case PTOK_A:
		case PTOK_C:
		case PTOK_D:
		case PTOK_F:
		case PTOK_R: return 1;
		default    : return 0;
	}
}

#if 0
static int handle_dot(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_at(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_colon(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_equ(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_plus(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_minus(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_gt(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_lt(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_pipe(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_lbrack(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_rbrack(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_comma(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_b(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_e(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_x(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_y(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_s(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_p(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_dquote(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_squote(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_hash(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_scolon(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_dollar(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_percent(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}
static int handle_number(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_alpha(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {

}

static int handle_other(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg) {
	return idx+1;
}

typedef int (*ptok_func)(char *str, int idx, uint8_t lex_type, line *l, token *t, uint8_t dbg);

static ptok_func ptok_handler[PTOK_OTHER+1] = {
	[PTOK_DOT    ] = handle_dot,
	[PTOK_AT     ] = handle_at,
	[PTOK_COLON  ] = handle_colon,
	[PTOK_EQU    ] = handle_equ,
	[PTOK_PLUS   ] = handle_plus,
	[PTOK_MINUS  ] = handle_minus,
	[PTOK_GT     ] = handle_gt,
	[PTOK_LT     ] = handle_lt,
	[PTOK_PIPE   ] = handle_pipe,
	[PTOK_LBRACK ] = handle_lbrack,
	[PTOK_RBRACK ] = handle_rbrack,
	[PTOK_COMMA  ] = handle_comma,
	[PTOK_B      ] = handle_b,
	[PTOK_E      ] = handle_e,
	[PTOK_X      ] = handle_x,
	[PTOK_Y      ] = handle_y,
	[PTOK_S      ] = handle_s,
	[PTOK_P      ] = handle_p,
	[PTOK_DQUOTE ] = handle_dquote,
	[PTOK_SQUOTE ] = handle_squote,
	[PTOK_HASH   ] = handle_hash,
	[PTOK_SCOLON ] = handle_scolon,
	[PTOK_DOLLAR ] = handle_dollar,
	[PTOK_PERCENT] = handle_percent,
	[PTOK_NUMBER ] = handle_number,
	[PTOK_ALPHA  ] = handle_alpha,
	[PTOK_OTHER  ] = handle_other
};
#endif
