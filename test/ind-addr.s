.org 0
rot:
	.qword 0

.org $1000
reset:
	cps
	ldb #1
	lda.w #rotate_left
	sta.q rot
	tba
main:
	jsr (rot)
	jmp main

rotate_left:
	rol b
	rts

rotate_right:
	ror b
	rts

.org $FFC0
.qword reset

a
d
