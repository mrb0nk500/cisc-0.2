.org 0
scr_ptr:
	.res 2
ptr:
	.res 8
ptr2:
	.res 8

.org $2000
buf:
	.res $2000

.org $8000
reset:
	cps		; Reset the processor status register.
	ldx.d #$2FFFF	; Reset the stack pointer.
	txs		;
@bench:
	tay		; Reset Y.
	lda.w #$1FFF	; Set the clear count to $1FFF.
	sta.w scr_ptr	;
	lda.d #buf	; Set the array to be cleared to the screen buffer.
	jsr clr_arr	; Clear the screen buffer.
	bra @bench

clr_arr:
	phb 		; Preserve whatever was in B.
	ldb #0		; Clear B.
	sta.q ptr	;
	adc #8		; Set the second pointer to the parameter, plus eight.
	sta.q ptr2	;
	tba		;
@loop:
	cpy.w scr_ptr	; Did we clear all of the array?
	bcs @end	; Yes, so we're done.
	sta.q (ptr), y	; No, so clear eight bytes.
	sta.q (ptr2), y	; Clear eight more bytes.
	tya		; Copy the array index.
	adc #$10	; Increment the index by 16.
	tay		; Update the index.
	tba		; Reset the Accumulator.
	sta.q (ptr), y	; Do this one more time, to clear 32 bytes.
	sta.q (ptr2), y	;
	tya		;
	adc #$10	;
	tay		;
	tba		;
	bra @loop	; Keep looping.
@end:
	tay		; Reset A.
	plb 		; Get whatever was in the B register, back.
	rts		; End of clr_arr.


;.org $C010
;.byte $1
.org $FFC0
.qword reset
a
d
