; Testing stack frames.
; Written by mr b0nk 500 <b0nk@b0nk.xyz>

.org $0
var:
	.byte 0

.org $8000
reset:
	cps		;
	ldx.d #$2FFFF	;
	txs		;
	lda #0		;
	tay		;
	tax		;
	tab		;
	sta.q var	;
start:
	inc		;
	pha		;
	ldb sp+1	;
	pla		;
	sta var		;
	ldy #var	;
	phy.q		;
	ldb (sp+1)	;
	ply.q		;
	ldb (sp-7)	;
	bra start	;

.org $FFC0
.qword reset
a
;.org reset
;v
;q
d
