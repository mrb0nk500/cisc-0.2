
.org 0
init:
	cps		; Clear the Processor Status register.
start:
	and #0		; Reset the accumulator.
	tab		;
	tax		;
	tay		;
	inb		;
	clc		;
fib:
	tya		;
	adc b		; Add x with y. But did we also carry over?
	bcs start	; Yes, so restart.
	tax		;
	tya		;
	tab		;
	txa		;
	tay		;
	bra fib		; No, so keep looping.
a
d
