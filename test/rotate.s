; Test rol, and ror.

.org 0
count:
	.res 1
tmp:
	.res 8

.org $8000
reset:
	cps		;
	ldx.d #$2FFFF	;
	txs		;
	and #0		;
	tax		;
	tab		;
	tay		;


start:
	ldb #8		; Set the loop count to 8.
@loop:
	lda #$AA	; Set A to magic byte.
	deb		; Decrement the loop count.
	beq @savemagic	; Save the magic number, if the loop count is zero.
	lsl #8		; Shift the magic number by one byte, if the loop count is non zero.
	bra @loop	; Keep looping.
@savemagic:
	sta.q tmp	; Save the magic number.


rotate:
	ldb #0		; Reset B.
@loop:
	and #0		; Reset A.
	lda #$40	; Set the shift count to 64.
	sta count	;
	lda.q tmp	; Get our magic number.
	inb		; Increment our shift amount by one.
	cpb #$40	; Is the shift amount greater than, or equal to 64?
	bcs rotate	; Yes, so reset B.
@rotl:
	rol b		; Rotate the magic number left by our shift amount.
	dec count	; Decrement the shift count.
	beq @rotr	; Start rotating right, if the shift count is zero.
	bra @rotl	; Keep looping, if the shift count is non zero.
@rotr:
	and #0		; Reset A.
	lda #$40	; Set the shift count to 64.
	sta count	;
	lda.q tmp	; Get our magic number.
@rotr2:
	ror b		; Rotate the magic number right by our shift amount.
	dec count	; Decrement the shift count.
	beq @loop	; Increment the shift amount, if the shift count is zero.
	bra @rotr2	; Keep looping, if the shift count is non zero.

.org $FFC0
.qword reset
a
d
