; Testing Signed shifts.
;
; Writen by mr b0nk 500 <b0nk@b0nk.xyz>

.org $8000
reset:
	cps
start:
	clc
	and #0
	dec
	tab
	lsl #$10
signshft:
	asr #1
	cmp b
	beq start
	bra signshft

.org $FFC0
.qword reset

a
d
