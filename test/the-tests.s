.org $0
init:
	cps

lstart:
	lda #1
	tab

lshft:
	llb
	bcs rstart
	jmp lshft

rstart:
	tba
	lsl #63

rshft:
	lrb
	bcs lstart
	jmp rshft

.org $FFC0
.qword init

.org $FF50
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
done
