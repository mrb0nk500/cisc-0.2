.org $0000
init:
cps

lstart:
lda #$01

lshft:
lsl #$01
bcs rstart
jmp lshft

rstart:
lda.q #$8000000000000000

rshft:
lsr #$1
bcs lstart
jmp rshft

.org $FFC0
.qword init

.org $FF50
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
.qword init
done

