; Name: fib.s
; Description: Computes the Fibbonacci sequence.
;
; Written in Sux Assembly
; by mr b0nk 500 <b0nk@b0nk.xyz>

; Variables for thread 0.
.org 0
x1:
	.res 8
y1:
	.res 8
z1:
	.res 8

; Variables for thread 1.
x2:
	.res 8
y2:
	.res 8
z2:
	.res 8

.org $8000
init:
	cps		; Clear the Processor Status register.

start:
	lda #$0		; Clear the accumulator.
	ldy #$1		; y=1.
	sty.q y1	; Store y into memory.

fib:
	ldx #$0		; x=0.
	ldx.q x1	; Output the value of x.
	adc.q y1	; Add x with y.
	sta.q z1	; z=x+y
	ldy.q y1
	sty.q x1	; x=y.
	sta.q y1	; y=z.
	lda.q x1
	bcs start	; Start all over again, if the carry flag was set.
	bra fib		; Otherwise, keep looping.


init2:
	cps		; Clear the Processor Status register.

start2:
	lda #$0		; Clear the accumulator.
	ldy #$1		; y2=1.
	sty.q y2	; Store y into memory.

fib2:
	ldx #$0		; x2=0.
	ldx.q x2	; Output the value of x2.
	adc.q y2	; Add x2 with y2.
	sta.q z2	; z2=x2+y2
	ldy.q y2
	sty.q x2	; x2=y2.
	sta.q y2	; y2=z2.
	lda.q x2
	bcs start2	; Start all over again, if the carry flag was set.
	bra fib2	; Otherwise, keep looping.


.org $FFC0
.qword init
; Set up the thread vectors.
.org $FF50
.qword init2
; Execute the program.
a
d
