.org 0
IMM   = 1 <<  0		; Immediate Data.
ZM    = 1 <<  1		; Zero Matrix.
ZMX   = 1 <<  2		; Zero Matrix, indexed with X.
ZMY   = 1 <<  3		; Zero Matrix, indexed with Y.
IND   = 1 <<  4		; Indirect.
INDX  = 1 <<  5		; Indexed Indirect.
INDY  = 1 <<  6		; Indirect Indexed.
ABS   = 1 <<  7		; Absolute.
REL   = 1 <<  8		; Relative.
BREG  = 1 <<  9		; B Register.
IMPL  = 1 << 10		; Implied.
INDX2 = 1 << 11		; Special case of INDX that uses the indirect table.
ZM2   = 1 << 12		; Special case of Zero Matrix used by JMP, and JSR.

inst:
	; ADC
	.word IMM|ZM|ABS|BREG
	.byte $01

	; AND
	.word IMM|ZM|ABS|BREG
	.byte $41

	; ASR
	.word IMM|ZM|ABS|BREG
	.byte $62

	; BCC
	.word REL
	.byte $A0

	; BCS
	.word REL
	.byte $90

	; BEQ
	.word REL
	.byte $B0

	; BNE
	.word REL
	.byte $C0

	; BNG
	.word REL
	.byte $80

	; BPO
	.word REL
	.byte $70

	; BRA
	.word REL
	.byte $F0

	; BRK
	.word IMPL
	.byte $69

	; BVC
	.word REL
	.byte $E0

	; BVS
	.word REL
	.byte $D0

	; CLC
	.word IMPL
	.byte $09

	; CLI
	.word IMPL
	.byte $29

	; CLV
	.word IMPL
	.byte $49

	; CMP
	.word IMM|ZM|IND|INDY|ABS|BREG|INDX2
	.byte $82

	; CPB
	.word IMM|ZM|IND|INDY|ABS|INDX2
	.byte $04

	; CPS
	.word IMPL
	.byte $00

	; CPX
	.word IMM|ZM|IND|ABS
	.byte $24

	; CPY
	.word IMM|ZM|IND|ABS
	.byte $44

	; DEB
	.word IMPL
	.byte $99

	; DEC
	.word IMPL|ZM|ABS
	.byte $84

	; DEX
	.word IMPL
	.byte $B9

	; DEY
	.word IMPL
	.byte $79

	; DIV
	.word IMM|ZM|ABS|BREG
	.byte $42

	; INB
	.word IMPL
	.byte $A9

	; INC
	.word IMPL|ZM|ABS
	.byte $A4

	; INX
	.word IMPL
	.byte $C9

	; INY
	.word IMPL
	.byte $89

	; JMP
	.word ABS|IND|ZM2
	.byte $00

	; JSR
	.word ABS|IND|ZM2
	.byte $20

	; LDA
	.word IMM|ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $C2

	; LDB
	.word IMM|ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $E2

	; LDX
	.word IMM|ZM|IND|ABS
	.byte $64

	; LDY
	.word IMM|ZM|IND|ABS
	.byte $A2

	; LSL
	.word IMM|ZM|ABS|BREG
	.byte $A1

	; LSR
	.word IMM|ZM|ABS|BREG
	.byte $C1

	; MUL
	.word IMM|ZM|ABS|BREG
	.byte $22

	; NOP
	.word IMPL
	.byte $EA

	; ORA
	.word IMM|ZM|ABS|BREG
	.byte $61

	; PHA
	.word IMPL
	.byte $8E

	; PHB
	.word IMPL
	.byte $AE

	; PHP
	.word IMPL
	.byte $6E

	; PHX
	.word IMPL
	.byte $EE

	; PHY
	.word IMPL
	.byte $CE

	; PLA
	.word IMPL
	.byte $9E

	; PLB
	.word IMPL
	.byte $BE

	; PLP
	.word IMPL
	.byte $7E

	; PLX
	.word IMPL
	.byte $FE

	; PLY
	.word IMPL
	.byte $DE

	; ROL
	.word IMM|ZM|ABS|BREG
	.byte $E1

	; ROR
	.word IMM|ZM|ABS|BREG
	.byte $02

	; RTI
	.word IMPL
	.byte $60

	; RTS
	.word IMPL
	.byte $50

	; SBC
	.word IMM|ZM|ABS|BREG
	.byte $21

	; SEC
	.word IMPL
	.byte $19

	; SEI
	.word IMPL
	.byte $39

	; STA
	.word ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $28

	; STB
	.word ZM|ZMX|ZMY|IND|INDX|INDY|ABS
	.byte $48

	; STX
	.word ZM|IND|ABS
	.byte $68

	; STY
	.word ZM|IND|ABS
	.byte $08

	; TAB
	.word IMPL
	.byte $0A

	; TAX
	.word IMPL
	.byte $4A

	; TAY
	.word IMPL
	.byte $2A

	; TBA
	.word IMPL
	.byte $1A

	; TSX
	.word IMPL
	.byte $8A

	; TXA
	.word IMPL
	.byte $5A

	; TXS
	.word IMPL|IMM
	.byte $9A

	; TXY
	.word IMPL
	.byte $7A

	; TYA
	.word IMPL
	.byte $3A

	; TYX
	.word IMPL
	.byte $6A

	; WAI
	.word IMPL
	.byte $59

	; XOR
	.word IMM|ZM|ABS|BREG
	.byte $81


a
.org 0
v
q
