; Test adding, and subtracting.
;
; Written by mr b0nk 500 <b0nk@b0nk.xyz>

; Main Program.
.org 0
reset:
	cps		; Clear the processor status register.
	ldx.d #$2FFFF	; Reset the stack pointer.
	txs		;
	and #0		; Reset A.
	tab		; Reset B.
	tax		; Reset X.
up:
	inc		; Increment the counter.
	cmp #$FF	; Did the accumulator overflow?
	bcs down	; Yes, so start decrementing.
	bra up		; No, so keep incrementing.
down:
	dec		; Did the accumulator underflow?
	beq up		; Yes, so start incrementing.
	bra down	; No, so keep decrementing.

.org $FFC0
.qword reset
a
d
