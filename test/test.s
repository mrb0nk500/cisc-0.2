; Testing Sux assembly, and the machine language monitor.
;
; Writen by mr b0nk 500 <b0nk@b0nk.xyz>
.org $8000
entry:
	cps		; Clear the processor status register.
	lda.w #$1000	; Load 0x1000 into the accumulator.
loop:
	rol #$1		; Then rotate the accumulator left by 1 bit.
	jmp loop	; And then loop.

.org $FFC0		; Reset vector.
.qword entry
a
d

