; Name: fib.s
; Description: Computes the Fibbonacci sequence.
;
; Written in Sux Assembly
; by mr b0nk 500 <b0nk@b0nk.xyz>

; Variables for thread 0.
.org 0
x1:
	.res 1
y1:
	.res 1
z1:
	.res 1

; Variables for thread 1.
x2:
	.res 1
y2:
	.res 1
z2:
	.res 1

.org $1000
init:
	cps		; Clear the Processor Status register.
start:
	lda #$0		; Clear the accumulator.
	ldy #$1		; y=1.
	sty y1		; Store y into memory.

fib:
	ldx #$0		; x=0.
	ldx x1		; Output the value of x.
	adc y1		; Add x with y.
	sta z1		; z=x+y
	ldy y1
	sty x1		; x=y.
	sta y1		; y=z.
	lda x1
	bcs start	; Start all over again, if the carry flag was set.
	bra fib		; Otherwise, keep looping.


init2:
	cps		; Clear the Processor Status register.
start2:
	lda #$0		; Clear the accumulator.
	ldy #$1		; y2=1.
	sty y2		; Store y into memory.

fib2:
	ldx #$0		; x2=0.
	ldx x2		; Output the value of x2.
	adc y2		; Add x2 with y2.
	sta z2		; z2=x2+y2
	ldy y2
	sty x2		; x2=y2.
	sta y2		; y2=z2.
	lda x2
	bcs start2	; Start all over again, if the carry flag was set.
	bra fib2	; Otherwise, keep looping.

.org $FFC0
.qword init
; Set up the thread vectors.
.org $FF50
.qword init2
; Execute the program.
a
d
