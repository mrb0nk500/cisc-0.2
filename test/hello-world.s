; Hello world.
; Writen in Sux assembly by mr b0nk 500 <b0nk@b0nk.xyz>

status	= $100		; Status.
scr	= $101		; Screen output.


; Initialize, and declare variables.
.org $A000
string:
	.byte "Hello, world!\n"

; Text buffer.
.org $2000
buffer:


; Get CPU into a known state.
.org $8000
reset:
	cps		; Reset the processor status.
	ldx.d #$2FFFF	; Set up the stack pointer.
	txs		; Reset the stack pointer.

; Start of main program.
start:
	ldx.w #$0	; Reset x.
	jmp print

line_count:
	iny		; Increment the line count.
	cpy #50		; Have we printed 50 lines?
	beq disp	; Yes, so we're done.
	jmp start	; No, so keep looping.

; Printing sub routine.
print:
	lda string, x	; Get character at offset x.
	beq line_count	; Did we find a null terminator?
	sta scr		; Print character.
	inx		; Increment offset.
	jmp print	; Keep printing more characters.

; The other threads would clash, if we're running the same code.
; So, have them spin instead, please?
disp:
	lda status
spin:
	nop
	nop
	nop
	jmp spin


.org $FFC0
.qword reset

.org $FF50
.qword spin
.qword spin
.qword spin
.qword spin
.qword spin
.qword spin
.qword spin
a
d
