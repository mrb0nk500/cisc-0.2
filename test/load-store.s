.org $8000

reset:
	cps
	ldx.d #$2FFFF
	txs
@clear:
	and #0
	tax
	tab
	deb
@loop:
	lsl #8
	lda #$FF
	cmp b
	beq @clear
	bra @loop

.org $FFC0
.qword reset
a
d
