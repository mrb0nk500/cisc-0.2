; Calculate population count/hamming weight.

.org $1000

popcnt:
	ldb #0		; Reset bit count.
	pha.q		; Create a temporary variable.
	cmp #0		; Set the status flags.
@loop:
	beq @end	; There are no more one bits left, so we're done.
	dec.q sp+1	; Decrement the temp variable.
	dec.q sp+1	; Decrement the temp variable.
	inb		; Increment the bit count.
	and.q sp+1	; AND value with value-1.
	sta.q sp+1	; Place it back in the temp variable.
	bra @loop	; Keep looping.
@end:
	pla.q		; Pull/Pop the temp variable off the stack.
	tba		; Return the bit count.
	rts		; End of popcnt.


reset:
	cps		; Boilerplate reset code.
	ldx.d #$2FFFF	;
	txs		;
	and #0		; Reset A.
	tab		; Reset B.
	tax		; Reset X.
	tay		; Reset Y.
main:
	pha.q		; Save A.
	jsr popcnt	; Get population count.
	tay		; Save it in Y.
	pla.q		; Get A back.
	inc		; Increment A by one.
	bra main	; Keep looping.


.org $FFC0
.qword reset

a
d
