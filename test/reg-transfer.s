; Test register transfer instructions.
;
; by mr b0nk 500 <b0nk @b0nk.xyz>

.org $8000
reset:
	cps
bench:
	inc		; Increment the accumulator.
	tay		; Transfer the accumulator to the y register.
	tax		; Do the same thing, but with the x register.
	tab		;
	bra bench	; Loop forever.

.org $FFC0
.qword reset

; Execute the program.
a
d
