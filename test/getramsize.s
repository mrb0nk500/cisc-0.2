; Find the end of RAM, and get the size of the RAM.
; Written by mr b0nk 500 <b0nk@b0nk.xyz>.

RAMSTART = $20000
MAGIC = $AA

.org 0
; End of RAM.
ptr1:
	.res 8

.org $8000
reset:
	cps		; Boilerplate reset code.
	ldx.d #$2FFFF	;
	txs		;
	and #0		; Reset A.
	tax		;
start:
	lda.d #RAMSTART	; Set the starting point of getramsize to the start of our RAM.
	jsr getramsize	; Get the size of the RAM.
	bra start	;


getramsize:
	sta.q ptr1	; Set the end of RAM pointer to the argument.
	and #0		; Reset A.
	tab		; Reset B.
	lda #MAGIC	; Set A to a magic number.
@loop:
	sta (ptr1)	; Write the magic number to the current end of RAM.
	cmp (ptr1)	; Is the value in RAM, the same as the magic number we wrote?
	bne @end	; No, so we're done.
	inc.q ptr1	; Yes, so increment the end of RAM pointer.
	inb		; Increment the byte count.
	bra @loop	; Keep looping.
@end:
	inc $0110	;
	rts		; End of getramsize.

.org $FFC0
.qword reset

a
d
