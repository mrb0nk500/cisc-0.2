; Register only version of popcnt.

.org $1000

popcnt:
	ldy #0		; Reset bit count.
	cmp #0		; Set the status flags.
@loop:
	beq @end	; There are no more one bits left, so we're done.
	tab		; Place the value in the B register.
	deb		; Decrement the temp value by one.
	iny		; Increment the bit count.
	and b		; AND value with value-1.
	bra @loop	; Keep looping.
@end:
	tya		; Return the bit count.
	rts		; End of popcnt.


reset:
	cps		; Boilerplate reset code.
	ldx.d #$2FFFF	;
	txs		;
	and #0		; Reset A.
	tab		; Reset B.
	tax		; Reset X.
	tay		; Reset Y.
main:
	pha.q		; Save A.
	jsr popcnt	; Get population count.
	tay		; Save it in Y.
	pla.q		; Get A back.
	inc		; Increment A by one.
	bra main	; Keep looping.


.org $FFC0
.qword reset

a
d
