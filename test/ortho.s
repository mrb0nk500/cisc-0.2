.org 0
count:
	.res 8
.org $40000
mem:
	.res 256
bitabl:
	.res 8
bits:
	.res 8

.org $8000
reset:
	cps
	lds.d #$3FFFF
	mov a, #10
	and #0
	tay
	xor b, b
	xor.q count, count
;	cmp.q (count), y
	cmp.qo (count), y
	lea.o (count), y
	lea mem
	nop
	lea d, $2000
	nop
	lea d, (mem+d)
	lea bitabl, mem
;	mov (bitabl)+a, 0
	mov (d-128), #0
	lea d, (8*a+e)
	lea s, count
	mov (e), #0
	mov a, (e)
	lea d, loop
	jmp (d)
loop:
	inc b
	inc.q count
;	mov.q e, count
	mov (b+e), b
	set a, eq
	mov a, (b+e)
	bra loop

.org $FFC0
.qword reset

a
.org reset
v
q
d
