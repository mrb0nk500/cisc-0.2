#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct tok {
	int skip; /* Number of blank tokens to skip. */
	char *value;
	struct tok *next;
};

typedef struct tok token;

struct line {
	int skip; /* Number of blank lines to skip. */
	token *tok;
	token *last_tok;
	struct line *next;
};

typedef struct line line;

extern token *tokens;
extern token *last_tok;
extern line *lines;
extern line *last_line;

extern int iseol(char c);
extern token *make_token(char *value);
extern line *get_line(char *str);
extern void parse_csv(FILE *fp);
extern void free_tokens(token *t, token *lt, int row);
extern void free_lines();
